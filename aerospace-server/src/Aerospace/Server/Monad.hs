module Aerospace.Server.Monad(
    AppMonad
  , AppPeer
  , module Aerospace.Server.Env
  ) where

import Aerospace.Network.Backend.Urho3D
import Aerospace.Server.Env
import Control.Monad.Reader
import Game.GoreAndAsh
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Sync

-- | Which network implementation to use
type AppNetworkBackend = UrhoNetworkBackend

-- | Peer connection for application monad
type AppPeer = Peer AppNetworkBackend

-- | Shortcut for monad stack that is not changing across server
type AppCoreMonad = SyncT Spider AppNetworkBackend (NetworkT Spider AppNetworkBackend (LoggingT Spider GMSpider))

-- | Application monad that is used for implementation of game API
type AppMonad = ReaderT (Env Spider AppCoreMonad) AppCoreMonad
