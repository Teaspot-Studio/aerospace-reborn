module Aerospace.Server.Urho(
    createApp
  ) where

import Aerospace.Server.Config
import Control.Monad.IO.Class
import Data.StateVar
import Foreign hiding (void)
import Graphics.Urho3D
import System.Directory (canonicalizePath)
import System.Environment (setEnv)

-- | Create headless urho that we need for network system
createApp :: MonadIO m => Config -> m (SharedPtr Application)
createApp Config{..} = do
  liftIO $ do
    p' <- canonicalizePath configResourcesPath
    setEnv "URHO3D_PREFIX_PATH" p'
  cntx <- newObject ()
  newSharedObject ApplicationCreate {
      appCreateContext = cntx
    , appCreateSetup = setupCallback
    , appCreateStart = startCallback
    , appCreateStop = stopCallback
    }
  where
    setupCallback app = do
      startupParameter app "Headless" $= True
    startCallback _ = pure ()
    stopCallback _ = pure ()
