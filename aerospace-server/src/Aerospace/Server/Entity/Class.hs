module Aerospace.Server.Entity.Class(
    EntityCreateReq
  , hoistEntityCreateReq
  , EntityCollectionConfig(..)
  , EntityCollection
  , EntityCollectionMonad(..)
  , ServerEntityConfig(..)
  , ServerEntity(..)
  , EntityCollectionEnv(..)
  , hoistEntityCollectionEnv
  , module Aerospace.Entity
  ) where

import Aerospace.Entity
import Aerospace.Reactive
import Aerospace.Server.Env.Class
import Control.Concurrent

-- | Lambda that is sent to entity collection
type EntityCreateReq t m = EntityId -> m (ServerEntity t)

-- | Transform monad the 'EntityCreateReq' works with
hoistEntityCreateReq :: (forall a . m a -> n a) -> EntityCreateReq t m -> EntityCreateReq t n
hoistEntityCreateReq = (.)

-- | Collection of entities input data
data EntityCollectionConfig t m = EntityCollectionConfig {
-- | Ask to add new entity to colelction and start simulation
  entityCollectionNew     :: !(Event t (EntityCreateReq t m))
-- | Remove entity from collection
, entityCollectionRemove  :: !(Event t EntityId)
}

-- | Collection that catches all entities on server
type EntityCollection t = Dynamic t (Map EntityId (ServerEntity t))

-- | Data input into entity component
data ServerEntityConfig t = ServerEntityConfig {
  sentityCfgId       :: !EntityId
  -- | Initial value and updates of position
, sentityCfgPosition :: !(Dynamic t Vector3)
  -- | Initial value and updates of velocity
, sentityCfgVelocity :: !(Dynamic t Vector3)
  -- | Initial rotation and updates for rotation
, sentityCfgRotation :: !(Dynamic t Vector3)
}

-- | Dynamic server side data about entity
data ServerEntity t = ServerEntity {
-- | Entity unique id
  sentityId       :: !EntityId
-- | Entity current position
, sentityPosition :: !(Dynamic t Vector3)
-- | Entity current velocity
, sentityVelocity :: !(Dynamic t Vector3)
-- | Entity current rotation
, sentityRotation :: !(Dynamic t Vector3)
-- | When entity is self descruted
, sentityDestruction :: !(Event t ())
}

-- | Monad where we have access to global entity collection
class EntityCollectionMonad t m | m -> t where
  -- | Creation of new entity
  createEntity :: Event t (EntityCreateReq t m) -> m (Event t (ServerEntity t))
  -- | Remove entity from simulation
  removeEntity :: Event t EntityId -> m (Event t ())
  -- | Get global entities
  entityCollection :: m (EntityCollection t)

-- | Internal environement of 'EntityCollectionMonad'
data EntityCollectionEnv t m = EntityCollectionEnv {
  envCreateEntity     :: !(EventWithTrigger t (EntityCreateReq t m))
, envRemoveEntity     :: !(EventWithTrigger t EntityId)
, envEntityCollection :: !(EntityCollection t)
}

-- | Transform monad that 'EntityCollectionEnv t m' works with. It looks like
-- a profunctor for 'm'.
hoistEntityCollectionEnv :: Reflex t
  => (forall a . m a -> n a) -- ^ Forward transform
  -> (forall a . n a -> m a) -- ^ Backward transform
  -> EntityCollectionEnv t m
  -> EntityCollectionEnv t n
hoistEntityCollectionEnv hoistForward hoistBackward env = EntityCollectionEnv {
    envCreateEntity = (fmap (hoistEntityCreateReq hoistForward) . fst . envCreateEntity $ env, (snd . envCreateEntity $ env) . hoistEntityCreateReq hoistBackward)
  , envRemoveEntity = envRemoveEntity env
  , envEntityCollection = envEntityCollection env
  }
