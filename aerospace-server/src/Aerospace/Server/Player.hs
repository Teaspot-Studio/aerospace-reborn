module Aerospace.Server.Player(
    PlayerCollection
  , playerCollection
  , ServerPlayer(..)
  , playerComponent
  ) where

import Aerospace.Player
import Aerospace.Reactive
import Aerospace.Server.Entity
import Aerospace.Server.Monad
import Data.Map.Strict (Map)
import Data.Maybe
import Data.Monoid

import qualified Data.Map.Strict as M

-- | Catches currently connected players collection
type PlayerCollection t = Dynamic t (Map PlayerId (ServerPlayer t))

-- | Handles players connection-disconnection and runs players logic code.
playerCollection :: forall t m . (ReactiveMonad t m, EntityCollectionMonad t m) => m (PlayerCollection t)
playerCollection = mdo
  connE <- peerConnected
  discE <- peerDisconnected
  let
    addE :: Event t (Map PlayerId (Maybe AppPeer))
    addE = flip pushAlways connE $ \peer -> do
      n <- sample . current $ countD
      pure $ M.singleton (PlayerId n) $ Just peer

    delExtE :: Event t (Map PlayerId (Maybe AppPeer))
    delExtE = flip push discE $ \peer -> do
      m <- sample . current $ peerMapD
      pure $ case M.lookup peer m of
        Nothing -> Nothing
        Just p -> Just $ M.singleton (splayerId p) Nothing

    delIntE :: Event t (Map PlayerId (Maybe AppPeer))
    delIntE = fmap (fmap $ const Nothing) $ switch . current $ mergeMap . fmap splayerRemove <$> fullMapD

    delE :: Event t (Map PlayerId (Maybe AppPeer))
    delE = delExtE <> delIntE

    updE :: Event t (Map PlayerId (Maybe AppPeer))
    updE = addE <> delE

    maker :: PlayerId -> AppPeer -> Event t AppPeer -> m (Event t (ServerPlayer t))
    maker i peer _ = playerComponent i peer

  tempMapD :: Dynamic t (Map PlayerId (Event t (ServerPlayer t))) <- listWithKeyShallowDiff mempty updE maker
  let countD :: Dynamic t Int
      countD = length <$> tempMapD

  fullMapD :: Dynamic t (Map PlayerId (ServerPlayer t)) <- holdDynMap tempMapD

  let peerMapD :: Dynamic t (Map AppPeer (ServerPlayer t))
      peerMapD = do
        pm <- fullMapD
        pure $ M.fromList . fmap (\p -> (splayerPeer p, p)) . M.elems $ pm

  -- Auto disconnect when deleted internally
  _ <- disconnectPeers $ flip pushAlways delIntE $ \m -> do
    fullMap <- sample . current $ fullMapD
    pure $ fmap splayerPeer . catMaybes . fmap (`M.lookup` fullMap) $ M.keys m

  logInfoE $ ffor addE $ \m -> "New players added: " <> showl (M.keys m)
  logInfoE $ ffor delE $ \m -> "Players removed: " <> showl (M.keys m)
  pure fullMapD


-- | Server side info about player
data ServerPlayer t = ServerPlayer {
  -- | Player id
  splayerId     :: !PlayerId
  -- | Entity info
, splayerEntity :: !(ServerEntity t)
  -- | Peer connection that is attached to the player
, splayerPeer   :: !AppPeer
  -- | Need to kick player and remove it from collection
, splayerRemove :: !(Event t ())
}

-- | Wrap peer to player and start simulation for him
playerComponent :: forall t m . (ReactiveMonad t m, EntityCollectionMonad t m)
  => PlayerId -> AppPeer -> m (Event t (ServerPlayer t))
playerComponent i peer = do
  buildE <- getPostBuild
  rec
    msgERaw <- receiveFromClient playerGlobalId peer
    msgE <- refireEvents msgERaw (void resE)
    playerD <- syncPlayer
    entityE <- createEntity $ ffor buildE $ const $ \ei -> entityComponent ServerEntityConfig {
        sentityCfgId = ei
      , sentityCfgPosition = playerPosition playerD
      , sentityCfgVelocity = playerVelocity playerD
      , sentityCfgRotation = playerRotation playerD
      }
    resE <- networkHoldE $ ffor entityE $ \entity -> do
      initE <- delay 0.01 =<< getPostBuild
      let respSelfId = fforMaybe msgE $ \case
            ReqSelfPlayerId -> Just (RespSelfPlayerId i (sentityId entity))
            _ -> Nothing
      -- logDebugE $ showl <$> respSelfId
      _ <- sendToClient playerGlobalId ReliableMessage respSelfId peer
      remEntDisconnect $ sentityId entity
      pure $ ffor initE $ const $ ServerPlayer {
          splayerId = i
        , splayerEntity = entity
        , splayerPeer = peer
        , splayerRemove = never
        }
  pure resE
  where
    defPlayer :: PlayerH
    defPlayer = Player {
        playerPosition = Vector3 2 2 2
      , playerVelocity = Vector3 0 0 0
      , playerRotation = Vector3 0 0 0
      }
    defPlayerD :: PlayerD t
    defPlayerD = Player {
        playerPosition = pure $ playerPosition defPlayer
      , playerVelocity = pure $ playerVelocity defPlayer
      , playerRotation = pure $ playerRotation defPlayer
      }

    syncPlayer :: m (PlayerD t)
    syncPlayer = fmap joinPlayer $ syncWithName (playerSyncName i) defPlayerD $ do
      let sync si a = fmap fst $ syncFromClient si (pure a) never peer
      playerPosition <- sync playerPositionId (playerPosition defPlayer)
      playerVelocity <- sync playerVelocityId (playerVelocity defPlayer)
      playerRotation <- sync playerRotationId (playerRotation defPlayer)
      pure Player{..}

    remEntDisconnect :: EntityId -> m ()
    remEntDisconnect ei = do
      discE <- peerDisconnected
      let ourDiscE = ffilter (peer ==) discE
      void $ removeEntity $ ei <$ ourDiscE
 
