module Aerospace.Server.Config(
    Config(..)
  , loadConfig
  ) where

import Aerospace.Aeson
import Control.Monad.IO.Class
import Data.Data
import Data.Text (Text)
import Data.Yaml.Config

import qualified Data.ByteString as BS

-- | Config file that stores saved options of application
data Config = Config {
-- | Which interface to listen
  configHost :: !Text
-- | Which port to listen
, configPort :: !Int
-- | Path to additional resources for internal Urho3D instance
, configResourcesPath :: !FilePath
} deriving (Show)

deriveJSON defaultOptions ''Config

-- | Load config from file and absolutize paths in int
loadConfig :: MonadIO m => FilePath -> m Config
loadConfig path = liftIO $ loadYamlSettings [path] [] useEnv
