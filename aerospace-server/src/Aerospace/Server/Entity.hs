module Aerospace.Server.Entity(
    module Aerospace.Server.Entity.Class
  , newEntityCollectionEnv
  , createEntityImpl
  , removeEntityImpl
  , entityCollectionComponent
  , entityComponent
  ) where

import Aerospace.Entity
import Aerospace.Reactive
import Aerospace.Server.Entity.Class
import Aerospace.Server.Env.Class
import Control.Concurrent

import qualified Data.Map.Strict as M

-- | Run implementation of 'EntityCollectionMonad'
newEntityCollectionEnv :: ReactiveMonad t m => m (EntityCollectionEnv t m)
newEntityCollectionEnv = do
  createEvT <- newTriggerEvent
  removeEvT <- newTriggerEvent
  es <- entityCollectionComponent EntityCollectionConfig {
      entityCollectionNew = fst createEvT
    , entityCollectionRemove = fst removeEvT
    }
  pure EntityCollectionEnv {
      envCreateEntity = createEvT
    , envRemoveEntity = removeEvT
    , envEntityCollection = es
    }

-- | Default implementation of 'createEntity' from 'EntityCollectionMonad'
createEntityImpl :: ReactiveMonad t m => EntityCollectionEnv t m -> Event t (EntityCreateReq t m) -> m (Event t (ServerEntity t))
createEntityImpl env reqE = do
  (ivar, ie) <- newReactiveMVar
  let mkWrapper f i = do
        res <- f i
        liftIO $ putMVar ivar $ sentityId res
        pure res
  let fire = snd . envCreateEntity $ env
  performEvent_ $ liftIO . fire . mkWrapper <$> reqE
  let watchOurEntity i = do
        buildE <- delay 0.01 =<< getPostBuild
        let es = envEntityCollection env
            checkE = leftmost [ buildE, void $ updated es ]
        pure $ flip push checkE $ const $ do
          m <- sample . current $ es
          pure $ M.lookup i m
  rec resE <- fmap (switch . current) $ networkHold (pure never) $ leftmost [
          watchOurEntity <$> ie
        , pure never <$ resE
        ]
  headE resE

-- | Default implementation of 'removeEntity' from 'EntityCollectionMonad'
removeEntityImpl :: ReactiveMonad t m => EntityCollectionEnv t m -> Event t EntityId -> m (Event t ())
removeEntityImpl env ei = do
  let fire = snd . envRemoveEntity $ env
      es = envEntityCollection env
  performEvent_ $ liftIO . fire <$> ei
  let watchOurEntity i = pure $ fforMaybe (updated es) $ \m -> case M.lookup i m of
        Nothing -> Just ()
        _ -> Nothing
  rec resE <- fmap (switch . current) $ networkHold (pure never) $ leftmost [
          watchOurEntity <$> ei
        , pure never <$ resE
        ]
  headE resE

-- | Component that controls global collection of entities
entityCollectionComponent :: forall t m . ReactiveMonad t m => EntityCollectionConfig t m -> m (EntityCollection t)
entityCollectionComponent EntityCollectionConfig{..} = mdo
  let
    addE :: Event t (Map EntityId (Maybe (EntityCreateReq t m)))
    addE = flip pushAlways entityCollectionNew $ \req -> do
      n <- sample . current $ countD
      pure $ M.singleton (EntityId n) $ Just req

    delExtE :: Event t (Map EntityId (Maybe (EntityCreateReq t m)))
    delExtE = (\i -> M.singleton i Nothing) <$> entityCollectionRemove

    delIntE :: Event t (Map EntityId (Maybe (EntityCreateReq t m)))
    delIntE = fmap (fmap $ const Nothing) $ switch . current $ mergeMap . fmap sentityDestruction <$> res

    delE :: Event t (Map EntityId (Maybe (EntityCreateReq t m)))
    delE = delExtE <> delIntE

    updE :: Event t (Map EntityId (Maybe (EntityCreateReq t m)))
    updE = addE <> delE

    maker :: EntityId -> (EntityCreateReq t m) -> Event t (EntityCreateReq t m) -> m (ServerEntity t)
    maker i f _ = f i
  res <- listWithKeyShallowDiff mempty updE maker
  let countD :: Dynamic t Int
      countD = length <$> res

  -- Send to clients about new and deleted entities
  let addMsgs = fmap NewEntityMessage . M.keys <$> addE
      delMsgs = fmap RemoveEntityMessage . M.keys <$> delE
  _ <- sendToAllClientsMany entityCollectionId ReliableMessage $ addMsgs <> delMsgs

  -- When requested, send entities around player to client
  void $ processPeers $ \peer -> do
    msgE <- receiveFromClient entityCollectionId peer
    let reqRespE = flip push msgE $ \case
          RequestEntitiesMessage -> do
            emap <- sample . current $ res
            pure . Just $ (\a -> [a]) . ResponseEntitiesMessage . M.keys $ emap
          _ -> pure Nothing
    let msgsE = reqRespE
    -- logDebugE $ showl <$> msgsE
    void $ sendToClientMany entityCollectionId ReliableMessage msgsE peer

  pure res

-- | Make logic that controls entities on server
entityComponent :: forall t m . ReactiveMonad t m => ServerEntityConfig t -> m (ServerEntity t)
entityComponent ServerEntityConfig{..} = do
  let sent = ServerEntity {
      sentityId = sentityCfgId
    , sentityPosition = sentityCfgPosition
    , sentityVelocity = sentityCfgVelocity
    , sentityRotation = sentityCfgRotation
    , sentityDestruction = never
    }
  syncEntity sent
  pure sent
  where
    syncEntity :: ServerEntity t -> m ()
    syncEntity ServerEntity{..} = void $ syncWithName (entitySyncName sentityId) () $ do
      let sync si a = do
            a' <- alignWithFps entitySyncFPS a
            void $ syncToAllClients si UnsequencedMessage a'
      sync entityPositionId sentityPosition
      sync entityVelocityId sentityVelocity
      sync entityRotationId sentityRotation
