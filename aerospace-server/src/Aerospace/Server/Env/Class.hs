module Aerospace.Server.Env.Class(
    ReactiveMonad
  , module Control.Monad.Fix
  , module Aerospace.Reactive
  , module Aerospace.Util
  , module Control.Monad.IO.Class
  , module Control.Monad.Reader
  , module Data.Map.Strict
  , module Data.Monoid
  , module Game.GoreAndAsh.Core.Monad
  , module Game.GoreAndAsh.Logging
  , module Game.GoreAndAsh.Network
  , module Game.GoreAndAsh.Sync
  , module Game.GoreAndAsh.Time
  , module Graphics.Urho3D.Math
  , module Reflex
  ) where

import Aerospace.Network.Backend.Urho3D
import Aerospace.Reactive
import Aerospace.Server.Config
import Aerospace.Server.Options
import Aerospace.Util
import Control.Monad.Fix
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Map.Strict (Map)
import Data.Monoid
import Data.Typeable (Typeable)
import Game.GoreAndAsh.Core.Monad
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Sync
import Game.GoreAndAsh.Time
import Graphics.Urho3D.Math hiding (constant)
import Reflex

-- | Shortcut for game functions that polymorphic around 't'
type ReactiveMonad t m = (
    Reflex t
  , MonadIO m
  , MonadFix m
  , MonadHold t m
  , MonadSample t m
  , Adjustable t m
  , PostBuild t m
  , MonadIO (Performable m)
  , PerformEvent t m
  , TriggerEvent t m
  , Typeable t
  , MonadGame t m
  , LoggingMonad t m
  , NetworkMonad t UrhoNetworkBackend m
  , NetworkClient t UrhoNetworkBackend m
  , NetworkServer t UrhoNetworkBackend m
  , SyncMonad t UrhoNetworkBackend m
  )
