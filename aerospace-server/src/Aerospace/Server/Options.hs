module Aerospace.Server.Options(
    Options(..)
  , parseOptions
  ) where

import Control.Monad.IO.Class
import Data.Semigroup ((<>))
import Data.Text (Text, pack)
import Options.Applicative

-- | Command line arguments to run server
data Options = Options {
-- | Where to read config file
  optionsConfigPath     :: !FilePath
}

-- | Command line arguments parser
optionsParser :: Parser Options
optionsParser = Options
  <$> strOption (
       long "config"
    <> metavar "CONFIG_PATH"
    <> showDefault
    <> help "Path to config file where game settings are located"
    <> value "./aerospace.yaml"
    )

-- | Read arguments from current program and parse them. Display help
-- message if asked.
parseOptions :: MonadIO m => m Options
parseOptions = liftIO . execParser $ info (optionsParser <**> helper) (
     fullDesc
  <> progDesc "Starts aerospace server"
  <> header "aerospace - small game inspired by sandboxes and roguelikes."
  )
