module Aerospace.Server.Env(
    Env(..)
  , hoistEnv
  , createEnv
  , module Aerospace.Server.Env.Class
  ) where

import Aerospace.Server.Config
import Aerospace.Server.Options
import Aerospace.Server.Env.Class
import Aerospace.Server.Entity
import Control.Monad.Fix
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Typeable (Typeable)
import Game.GoreAndAsh.Core.Monad
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Network.Backend.TCP
import Game.GoreAndAsh.Sync
import Graphics.Urho3D.Math hiding (constant)
import Reflex

import qualified Data.Text as T

-- | Internal environment that can be accessed by game functions
data Env t m = Env {
  envOptions        :: !Options
, envConfig         :: !Config
, envEntityCollEnv  :: !(EntityCollectionEnv t m)
}

-- | Transform monad that 'EntityCollectionEnv t m' works with. It looks like
-- a profunctor for 'm'.
hoistEnv :: Reflex t
  => (forall a . m a -> n a) -- ^ Forward transform
  -> (forall a . n a -> m a) -- ^ Backward transform
  -> Env t m
  -> Env t n
hoistEnv hf hb e = e {
    envOptions = envOptions e
  , envConfig = envConfig e
  , envEntityCollEnv = hoistEntityCollectionEnv hf hb $ envEntityCollEnv e
  }

-- | Initialize environment for the given scope
createEnv :: ReactiveMonad t m => Options -> m (Env t m)
createEnv opts@Options{..} = do
  cfg <- loadConfig optionsConfigPath
  ecEnv <- newEntityCollectionEnv
  let
    env = Env {
        envOptions = opts
      , envConfig = cfg
      , envEntityCollEnv = ecEnv
      }
  pure env

instance ReactiveMonad t m => EntityCollectionMonad t (ReaderT (Env t m) m) where
  createEntity e = do
    env <- ask
    let env' = hoistEntityCollectionEnv (ReaderT . const) (flip runReaderT env) $ envEntityCollEnv env
    createEntityImpl env' e
  {-# INLINE createEntity #-}

  removeEntity e = do
    env <- ask
    let env' = hoistEntityCollectionEnv (ReaderT . const) (flip runReaderT env) $ envEntityCollEnv env
    removeEntityImpl env' e
  {-# INLINE removeEntity #-}

  entityCollection = asks $ envEntityCollection . envEntityCollEnv
  {-# INLINE entityCollection #-}
