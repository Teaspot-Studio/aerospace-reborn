module Aerospace.Server(
    startAerospace
  , Options(..)
  , parseOptions
  ) where

import Aerospace.Network.Backend.Urho3D
import Aerospace.Server.Config
import Aerospace.Server.Entity
import Aerospace.Server.Env
import Aerospace.Server.Monad
import Aerospace.Server.Options
import Aerospace.Server.Player
import Aerospace.Server.Urho
import Control.Concurrent
import Control.Lens
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Maybe
import Data.Monoid
import Game.GoreAndAsh
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Sync
import Game.GoreAndAsh.Time
import Graphics.Urho3D (applicationRun)

import qualified Data.Text as T

-- | Start aerospace server with given options
startAerospace :: MonadIO m => Options -> m ()
startAerospace opts@Options{..} = liftIO . withNetwork $ do
  cfg <- loadConfig optionsConfigPath
  app <- createApp cfg
  _ <- liftIO . forkIO $ do
    mres <- runGM $ do
      runLoggerT . runNetworkT (netopts app cfg) . runSyncT sopts $ do
        env <- createEnv opts
        flip runReaderT env $ serverGame
    case mres of
      Left er -> print $ renderNetworkError er
      Right _ -> pure ()
  applicationRun app
  where
    sopts = defaultSyncOptions & syncOptionsRole .~ SyncMaster
    tcpOpts app Config{..} = UrhoNetworkOpts {
        urhoNetworkApp = app
      , urhoNetworkServer = Just configPort
      , urhoNetworkSimulatedLatency = Nothing -- Just 100
      , urhoNetworkSimulatedLoss = Nothing -- Just 0.5
      }
    netopts app cfg = (defaultNetworkOptions $ tcpOpts app cfg) {
        networkOptsDetailedLogging = False
      }

    serverGame :: AppMonad ()
    serverGame = do
      Config{..} <- asks envConfig
      e <- getPostBuild
      logInfoE $ ffor e $ const $ "Started to listen port " <> showl configPort <> " ..."

      connE <- peerConnected
      logInfoE $ ffor connE $ const $ "Peer is connected..."

      discE <- peerDisconnected
      logInfoE $ ffor discE $ const $ "Peer is disconnected..."

      someErrorE <- networkSomeError
      sendErrorE <- networkSendError
      logWarnE $ ffor someErrorE $ \er -> "Network error: " <> showl er
      logWarnE $ ffor sendErrorE $ \er -> "Network send error: " <> showl er

      _ <- playerCollection

      -- _ <- playGame
      return ()
