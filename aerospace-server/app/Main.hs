module Main where

import Aerospace.Server

main :: IO ()
main = startAerospace =<< parseOptions
