module Main where

import Aerospace.Client

main :: IO ()
main = startAerospace =<< parseOptions
