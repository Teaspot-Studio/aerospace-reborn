module Aerospace.Client(
    startAerospace
  , Options(..)
  , parseOptions
  ) where

import Aerospace.Client.Camera
import Aerospace.Client.Entity
import Aerospace.Client.Env
import Aerospace.Client.Env.Reactive
import Aerospace.Client.Env.Reactive.Impl
import Aerospace.Client.Human
import Aerospace.Client.Input
import Aerospace.Client.Monad
import Aerospace.Client.Options
import Aerospace.Client.Player
import Aerospace.Client.Viewport
import Aerospace.Network.Backend.Urho3D
import Aerospace.Vector3
import Control.Concurrent
import Control.Lens
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Maybe
import Data.Monoid
import Game.GoreAndAsh
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Sync
import Game.GoreAndAsh.Time
import Graphics.Urho3D hiding (Event)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Text as T

-- | Start aerospace client with given options
startAerospace :: MonadIO m => Options -> m ()
startAerospace opts@Options{..} = liftIO . withNetwork $ do
  env <- createEnv opts
  let app = envApplication env
  _ <- liftIO . forkIO $ do
    mres <- runGM $ runLoggerT . runNetworkT (netopts app) . runSyncT sopts . flip runReaderT env $ initClientGame
    case mres of
      Left er -> print $ renderNetworkError er
      Right _ -> pure ()
  runEnvApplication env
  where
    sopts = defaultSyncOptions & syncOptionsRole .~ SyncSlave
    tcpOpts app = UrhoNetworkOpts {
        urhoNetworkApp =  app
      , urhoNetworkServer = Nothing
      , urhoNetworkSimulatedLatency = Nothing -- Just 100
      , urhoNetworkSimulatedLoss = Nothing -- Just 0.5
      }
    netopts app = (defaultNetworkOptions $ tcpOpts app) {
        networkOptsDetailedLogging = False
      }

    initClientGame :: InitAppMonad ()
    initClientGame = do
      initE <- initReadyEvent
      void $ networkHold (pure ()) $ ffor initE $ const $ do
        env <- ask
        renv <- newReactiveEnv env
        ReaderT . const $ runReaderT clientGame renv

    clientGame :: AppMonad ()
    clientGame = do
      loggingSetDebugFlag False
      e <- getPostBuild
      let connOpts = UrhoNetworkConnectOpts {
              urhoNetworkConnectPort = fromMaybe 5544 optionsServerPort
            }
          connAddr = BS.pack . T.unpack $ fromMaybe "127.0.0.1" optionsServerHost
      connectedE <- clientConnect $ ffor e $ const (connAddr, connOpts)
      conErrorE <- networkConnectionError
      logInfoE $ ffor connectedE $ const "Connected to server!"
      logErrorE $ ffor conErrorE $ \er -> "Failed to connect: " <> showl er
      _ <- networkHold (pure ()) $ ffor connectedE $ const playPhase
      return ()

    playPhase :: AppMonad ()
    playPhase = do
      playerE <- playerComponent
      networkWith_ playerE $ \player -> do
        startupViewSetup $ cplayerCamera player
        let ccam = cplayerCamera player
        modelNode <- nodeCreateChild (cameraNode ccam) "Human test" CMLocal 0
        nodeSetPosition modelNode (Vector3 0 (-3) 8)
        -- spaceD <- inputKeyIsDown ScancodeSpace
        _ <- humanComponent HumanCfg {
            humanCfgModelCfg = HumanModelConfig {
                humanModelCfgGender = Male
              }
          , humanCfgMovingSpeed = cameraEyeVel ccam -- ffor spaceD $ \v -> if v then Vector3 0 0 20 else 0
          , humanCfgViewDir = eulerToDirection <$> cameraAngles ccam -- pure $ normalizev $ Vector3 1 0 0
          , humanCfgParentNode = modelNode
          }
        pure ()
      pure ()

    startupViewSetup :: CameraComponent t -> AppMonad ()
    startupViewSetup cam = do
      _ <- initViewport ViewportConfig {
          viewportCfgCamera = pure $ cameraPtr cam
        }
      pure ()
