module Aerospace.Client.UI(
    getUIFocusChange
  , getUIFocused
  , getCursorVisible
  ) where

import Aerospace.Client.Monad
import Aerospace.Client.Env.Reactive
import Aerospace.Util
import Foreign hiding (void)
import Game.GoreAndAsh
import Graphics.Urho3D hiding (Event)

-- | Get event that fires when UI changes focus
getUIFocusChange :: ReactiveMonad t m => m (Event t (Maybe (Ptr UIElement)))
getUIFocusChange = autoCachedEvent "getUIFocusChange" $ do
  ui <- getUI
  updE <- getUpdateEvent
  performMainThread $ ffor updE $ const $ uiFocusElement ui

-- | Get currently focused element
getUIFocused :: ReactiveMonad t m => m (Dynamic t (Maybe (Ptr UIElement)))
getUIFocused = autoCachedDynamic "getUIFocused" $ holdDyn Nothing =<< getUIFocusChange

-- | Get current visibility of cursor
getCursorVisible :: ReactiveMonad t m => m (Dynamic t Bool)
getCursorVisible = autoCachedDynamic "getCursorVisible" $ do
  cursor <- getCursor
  updE <- getUpdateEvent
  visibleE <- performMainThread $ ffor updE $ const $ uiElementIsVisible cursor
  holdDyn False visibleE
