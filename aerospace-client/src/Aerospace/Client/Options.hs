module Aerospace.Client.Options(
    Options(..)
  , parseOptions
  ) where

import Control.Monad.IO.Class
import Data.Semigroup ((<>))
import Data.Text (Text, pack)
import Options.Applicative

-- | Command line arguments to run client
data Options = Options {
-- | Server host to automatically connect to
  optionsServerHost     :: !(Maybe Text)
-- | Server port to automatically connect to
, optionsServerPort     :: !(Maybe Int)
-- | Path to folder with models/textures/other files
, optionsResourcesPath  :: !FilePath
-- | Where to write execution logs
, optionsLogPath        :: !FilePath
-- | Where to read config file
, optionsConfigPath     :: !FilePath
-- | If set, dumps used resources on exit
, optionsConfigDump     :: !Bool
}

-- | Command line arguments parser
optionsParser :: Parser Options
optionsParser = Options
  <$> (optional . textOption) (
       long "host"
    <> metavar "SERVER_ADDRESS"
    <> help "Server host to automatically connect to"
    )
  <*> (optional . option auto) (
       long "port"
    <> metavar "SERVER_PORT"
    <> help "Server port to automatically connect to"
    )
  <*> strOption (
       long "data"
    <> metavar "RESOURCES_DATA_DIR"
    <> showDefault
    <> help "Path to folder with models/textures/other files"
    <> value "../resources"
    )
  <*> strOption (
       long "log"
    <> metavar "LOG_PATH"
    <> showDefault
    <> help "Path to file where to write logs"
    <> value "./aerospace.log"
    )
  <*> strOption (
       long "config"
    <> metavar "CONFIG_PATH"
    <> showDefault
    <> help "Path to config file where game settings are located"
    <> value "./aerospace.yaml"
    )
  <*> switch (
       long "dump-resources"
    <> showDefault
    <> help "If set prints all used resources on game exit (debug feature)"
    )

-- | Helper to allow parse options in 'Text' directly
textOption :: Mod OptionFields String -> Parser Text
textOption = fmap pack . strOption

-- | Read arguments from current program and parse them. Display help
-- message if asked.
parseOptions :: MonadIO m => m Options
parseOptions = liftIO . execParser $ info (optionsParser <**> helper) (
     fullDesc
  <> progDesc "Starts aerospace game"
  <> header "aerospace - small game inspired by sandboxes and roguelikes."
  )
