module Aerospace.Client.Human(
    HumanCfg(..)
  , HumanModelConfig(..)
  , Gender(..)
  , HumanComponent(..)
  , humanComponent
  ) where

import Aerospace.Client.Entity
import Aerospace.Client.Env.Reactive
import Aerospace.Client.Model.Human
import Aerospace.Quaternion
import Aerospace.Vector3
import Control.Monad.Except
import Data.Proxy
import Graphics.Urho3D.Multithread
import Reflex.Workflow

import Debug.Trace

-- | Configuration of human model
data HumanCfg t = HumanCfg {
  -- | Appereance of human model
    humanCfgModelCfg    :: HumanModelConfig
  -- | Current speed to play anymations
  , humanCfgMovingSpeed :: !(Dynamic t Vector3)
  -- | View direction of the human
  , humanCfgViewDir     :: !(Dynamic t Vector3)
  -- | Parent node to attach human model to
  , humanCfgParentNode  :: !(Ptr Node)
}

-- | Outputs of human model
data HumanComponent t = HumanComponent {

}

data WalkingState = Walking Vector3 | Standing

-- | Controller for leg and hands animation
animateLimp :: forall t m . (ReactiveMonad t m)
  => Bool -> Ptr Node -> Vector3 -> Dynamic t WalkingState -> m ()
animateLimp initDir legNode joinPoint walkingD = void . workflow $ runningStage
  where
  standingStage :: Workflow t m ()
  standingStage = Workflow $ do
    let nextStageE = fforMaybe (updated walkingD) $ \case
          Walking _ -> Just ()
          _ -> Nothing
    pure ((), runningStage <$ nextStageE)

  runningStage :: Workflow t m ()
  runningStage = Workflow $ mdo
    updE <- getUpdateEvent
    let angSpd = 0.2
        maxAng = pi/6
        dangleE = flip pushAlways (_eventUpdateTimeStep <$> updE) $ \dt -> do
          walk <- sample . current $ walkingD
          case walk of
            Walking v -> do
              let spd = lengthv v
              dir <- sample . current $ dirD
              pure $ (if dir then 1 else -1) * spd * angSpd * dt
            Standing -> do
              ang <- sample . current $ angD
              pure $ negate (signum ang) * 20 * angSpd * dt
        nextStageE = flip push (updated angD) $ \ang -> do
          walk <- sample . current $ walkingD
          pure $ case walk of
            Standing -> if abs ang < 0.01 then Just () else Nothing
            _ -> Nothing
    angD <- foldDyn (\da a -> a + da) 0 dangleE
    let updDir a dir = if dir
          then if a > maxAng then not dir else dir
          else if a < negate maxAng then not dir else dir
    dirD <- foldDyn updDir initDir $ updated angD

    dangleVar <- liftIO $ newIORef Nothing
    performEvent_ $ ffor dangleE $ \da -> liftIO $ atomicWriteIORef dangleVar $ Just da
    liftIO $ addMainThreadWorker $ do
      mda <- atomicModifyIORef dangleVar $ \da -> (Nothing, da)
      whenJust mda $ \da -> 
        nodeRotateAround legNode joinPoint (quaternionFromAxis (Vector3 1 0 0) da) TSLocal
      pure True

    pure ((), standingStage <$ nextStageE)

-- | Controller for rotation of body
animateBody :: forall t m . (ReactiveMonad t m)
  => Ptr Node -> Dynamic t Vector3 -> Dynamic t WalkingState -> m ()
animateBody bodyNode dirD walkingD = void . workflow $ runningStage (Vector3 0 0 0) -- TODO: init forward
  where
  standingStage :: Vector3 -> Workflow t m ()
  standingStage initForward = Workflow $ do
    updE <- getUpdateEvent
    forwardE <- performEvent $ ffor updE $ const $ nodeGetDirection bodyNode
    forwardD <- holdDyn initForward forwardE
    let upVector = Vector3 0 1 0 -- TODO: up vector
        angSpd = 4
        maxAng = pi/6
        dangleE = flip push (_eventUpdateTimeStep <$> updE) $ \dt -> do
          walk <- sample . current $ walkingD
          case walk of
            Standing -> do
              forwardv <- sample . current $ forwardD
              dir <- sample . current $ dirD
              let da = signedAngle upVector forwardv dir
              -- traceShowM da
              pure $ if abs da > maxAng
                then Just $ da * angSpd * dt
                else Nothing
            Walking _ -> pure Nothing

        nextStageE = flip push (updated walkingD) $ \case
          Walking _ -> do
            forward <- sample . current $ forwardD
            pure $ Just forward
          _ -> pure Nothing

    performMainThread_ $ ffor dangleE $ \da -> do
      nodeRotate bodyNode (quaternionFromAxis upVector da) TSLocal

    pure ((), runningStage <$> nextStageE)

  runningStage :: Vector3 ->  Workflow t m ()
  runningStage initForward = Workflow $ mdo
    updE <- getUpdateEvent
    forwardE <- performEvent $ ffor updE $ const $ nodeGetDirection bodyNode
    forwardD <- holdDyn initForward forwardE
    let upVector = Vector3 0 1 0 -- TODO: up vector
        angSpd = 8
        dangleE = flip push (_eventUpdateTimeStep <$> updE) $ \dt -> do
          walk <- sample . current $ walkingD
          case walk of
            Walking v -> do
              forwardv <- sample . current $ forwardD
              let da = signedAngle upVector forwardv v
              pure $ if abs da < 0.01 then Nothing else Just $ da * angSpd * dt
            Standing -> pure Nothing

        nextStageE = flip push (updated walkingD) $ \case
          Standing -> do
            forward <- sample . current $ forwardD
            pure $ Just forward
          _ -> pure  Nothing

    performMainThread_ $ ffor dangleE $ \da -> do
      nodeRotate bodyNode (quaternionFromAxis upVector da) TSLocal

    pure ((), standingStage <$> nextStageE)

-- | Create client player and start synching and prediction
humanComponent :: forall t m . (ReactiveMonad t m, EntityCollectionMonad t m)
  => HumanCfg t -> m (Event t (HumanComponent t))
humanComponent HumanCfg{..} = do
  buildE <- getPostBuild
  modelE <- performMainThread $ ffor buildE $ const $ runExceptT $ humanModel humanCfgModelCfg humanCfgParentNode
  networkWith_ modelE $ \case
    Left er -> liftIO $ putStrLn $ show er
    Right model -> do
      let walkingD = ffor humanCfgMovingSpeed $ \v -> if lengthv v > 0.5 then Walking v else Standing
      uncurry (animateLimp True) (humanModelLeftLeg model) walkingD
      uncurry (animateLimp False) (humanModelRightLeg model) walkingD
      uncurry (animateLimp False) (humanModelLeftHand model) walkingD
      uncurry (animateLimp True) (humanModelRightHand model) walkingD
      animateBody (humanModelBody model) humanCfgViewDir walkingD
    -- let animateE = flip push updE $ \upd -> do
    --       let t = _eventUpdateTimeStep upd
    --       isWalking <- sample humanCfgWalking
    --       pure $ if isWalking then Just t else Nothing
    -- runInMainThread_ $ ffor animateE $ \dt -> do

  pure $ HumanComponent <$ buildE
