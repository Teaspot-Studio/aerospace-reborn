module Aerospace.Client.Entity.Class(
    EntityCollection
  , EntityCollectionMonad(..)
  , ClientEntityCfg(..)
  , ClientEntity(..)
  , EntityCollectionEnv(..)
  , module Aerospace.Entity
  ) where

import Aerospace.Entity
import Data.Map.Strict (Map)
import Foreign
import Graphics.Urho3D hiding (Event, constant, scale)
import Reflex

-- | Known entities on the client side
type EntityCollection t = Dynamic t (Map EntityId (ClientEntity t))

-- | API to work with entity collection
class EntityCollectionMonad t m | m -> t where
  -- | Get entity component now or once it added in future
  waitEntity :: Event t EntityId -> m (Event t (ClientEntity t))

-- | Data required to create new entity
data ClientEntityCfg t = ClientEntityCfg {
  -- | Unique server id of entity
  centityCfgId     :: !EntityId
  -- | Attach the entity to the other node (or global scene)
, centityCfgParent :: !(Maybe (Ptr Node))
  -- | Event that is fired when the entity is removed from collection
, centityCfgRemove :: !(Event t ())
  -- | Fast ticker to update predictions of entity
, centityCfgTick   :: !(Event t Float)
}

-- | Client side info about entity
data ClientEntity t = ClientEntity {
-- | Entity unique id
  centityId       :: !EntityId
-- | Entity current position
, centityPosition :: !(Dynamic t Vector3)
-- | Entity current velocity
, centityVelocity :: !(Dynamic t Vector3)
-- | Entity current rotation
, centityRotation :: !(Dynamic t Vector3)
-- | Entity scene node
, centityNode     :: !(Ptr Node)
}

-- | Internal environement for 'EntityCollectionMonad'
data EntityCollectionEnv t = EntityCollectionEnv {
  envEntityCollection :: !(EntityCollection t)
}
