{-# LANGUAGE OverloadedLists #-}
module Aerospace.Client.Model.Custom(
    solidCustomModel
  ) where

import Aerospace.CSG
import Aerospace.Material
import Aerospace.Quaternion
import Aerospace.Triangle
import Aerospace.Urho
import Aerospace.Util
import Aerospace.Vertex
import Control.Monad.Except
import Data.Semigroup
import Data.Store.Core
import Foreign hiding (void)
import Graphics.Urho3D hiding (Sphere, Material, HasContext)

import qualified Data.ByteString.Unsafe as BS
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as VU

-- | Encode buffer and use it in context
withBuffer :: Poke () -> Int -> (Ptr () -> IO a) -> IO a
withBuffer p n io = BS.unsafeUseAsCString (unsafeEncodeWith p n) $ io . castPtr

-- | Load triangles into buffers and create geometry
trianglesGeometry :: (MonadIO m, HasContext m)
  => VU.Vector (VertexTriangle ())
  -> m (SharedPtr VertexBuffer, SharedPtr IndexBuffer, SharedPtr Geometry)
trianglesGeometry trs = do
  ctx <- getUrhoContext
  vb :: SharedPtr VertexBuffer <- newSharedObject $ pointer ctx
  ib :: SharedPtr IndexBuffer <- newSharedObject $ pointer ctx
  geom :: SharedPtr Geometry  <- newSharedObject $ pointer ctx
  let numVertices = fromIntegral $ 3 * VU.length trs
      vertexSize = 2 * sizeOf (undefined :: Vector3) + sizeOf (undefined :: Vector2)

  -- Shadowed buffer needed for raycasts to work, and so that data can be automatically restored on device loss
  vertexBufferSetShadowed vb True
  let elements = [ vertexElement TypeVector3 SEMPosition
                 , vertexElement TypeVector3 SEMNormal
                 , vertexElement TypeVector2 SEMTexCoord ]
  _ <- vertexBufferSetSize vb numVertices elements False
  let mkBuffer = VU.mapM_ storeRow trs
      storeVertex (Vertex p n t) = do
        pokeStorable p
        pokeStorable n
        pokeStorable t
      storeRow (VertexTriangle v1 v2 v3 _) = do
        storeVertex v1
        storeVertex v2
        storeVertex v3
  _ <- liftIO $ withBuffer mkBuffer (fromIntegral numVertices * vertexSize) $ vertexBufferSetData vb

  indexBufferSetShadowed ib True
  _ <- indexBufferSetSize ib numVertices False False
  let mindecies = [(0 :: Word16) .. fromIntegral numVertices - 1]
  let mkIndexBuffer = V.mapM_ pokeStorable mindecies
  _ <- liftIO $ withBuffer mkIndexBuffer (fromIntegral numVertices * sizeOf (undefined :: Word16)) $ indexBufferSetData ib

  _ <- geometrySetVertexBuffer geom 0 (pointer vb)
  geometrySetIndexBuffer geom (pointer ib)
  _ <- geometrySetDrawRange geom TriangleList 0 numVertices True

  pure (vb, ib, geom)

-- | Convert CSG triangles into Urho3D model
solidCustomModel :: (MonadIO m, HasContext m, HasMaterials m, HasResourceCache m, MonadError UrhoError m)
  => Ptr Node -> Solid Material -> m (Ptr Node)
solidCustomModel pnode csg = do
  ctx <- getUrhoContext
  mats <- askMaterials
  let layers = M.toList $ renderSolid $ materialLocalId <$> csg

  mbuffers <- forM layers $ \(i, trs) -> do
    mat <- maybe (throwError $ MissingMaterialLocalId i) pure $ lookupLocalMaterial i mats
    emat <- createEngineMaterial mat
    (vb, ib, geom) <- trianglesGeometry $ VU.map void trs
    pure (vb, ib, geom, emat)

  cmodel :: SharedPtr Model <- newSharedObject $ pointer ctx

  _ <- modelSetNumGeometries cmodel $ fromIntegral $ length mbuffers
  forM_ ([0 ..] `zip` mbuffers) $ \(i, (_, _, geom, _)) -> void $ modelSetGeometry cmodel i 0 (pointer geom)
  modelSetBoundingBox cmodel $ BoundingBox (Vector3 (-1) (-1) (-1)) (Vector3 1 1 1) -- TODO

  -- Though not necessary to render, the vertex & index buffers must be listed in the model so that it can be saved properly
  let vertexBuffers = V.fromList $ (\(vb, _, _, _) -> vb) <$> mbuffers
      indexBuffers = V.fromList $ (\(_, ib, _, _) -> ib) <$> mbuffers
  -- Morph ranges could also be not defined. Here we simply define a zero range (no morphing) for the vertex buffer
      morphRangeStarts = V.fromList $ 0 <$ mbuffers
      morphRangeCounts = V.fromList $ 0 <$ mbuffers
  _ <- modelSetVertexBuffers cmodel vertexBuffers morphRangeStarts morphRangeCounts
  _ <- modelSetIndexBuffers cmodel indexBuffers

  modelNode <- nodeCreateChild pnode "ProceduralMesh" CMReplicated 0
  object :: Ptr StaticModel <- guardNothing "ProceduralMeshModel" =<< nodeCreateComponent modelNode Nothing Nothing
  staticModelSetModel object cmodel
  forM_ ([0 ..] `zip` mbuffers) $ \(i, (_, _, _, emat)) -> void $ staticModelSetGeometryMaterial object i emat
  drawableSetCastShadows object True
  pure modelNode
