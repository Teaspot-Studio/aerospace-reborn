module Aerospace.Client.Model.Human(
    Gender(..)
  , HumanModelConfig(..)
  , HumanModel(..)
  , humanModel
  ) where

import Aerospace.Client.Model.Custom
import Aerospace.CSG
import Aerospace.Material
import Aerospace.Quaternion
import Aerospace.Triangle
import Aerospace.Urho
import Aerospace.Util
import Aerospace.Vertex
import Control.Monad.Except
import Data.Semigroup
import Data.Store.Core
import Foreign hiding (void)
import Graphics.Urho3D hiding (Sphere, Material, HasContext)

-- | Biological gender
data Gender = Male | Female
  deriving (Eq, Ord, Show, Enum, Bounded)

-- | Here goes configuration of human visual properties.
--
-- Gender, height, fatness, skin color and e.t.c.
data HumanModelConfig = HumanModelConfig {
-- | Biological gender
  humanModelCfgGender :: !Gender
} deriving (Show)

bodySolid :: HasMaterials m => HumanModelConfig -> m (Solid Material)
bodySolid HumanModelConfig{..} = do
  boob <- boobsSolid
  mats <- askMaterials
  let Just skin = lookupGlobalMaterial (materialId skinMaterial) mats
  let body = Scaled (Vector3 1 1.5 0.5) $ cube skin
      boobs = (Translated (Vector3 (-0.25) 0.4 (-0.25)) boob) <> (Translated (Vector3 (0.25) 0.4 (-0.25)) boob)
  pure $ case humanModelCfgGender of
    Male -> body
    Female -> boobs <> body

headSolid :: HasMaterials m => m (Solid Material)
headSolid = do
  mats <- askMaterials
  let Just skin = lookupGlobalMaterial (materialId skinMaterial) mats
  pure $ Scaled (Vector3 0.7 0.7 0.7) $ cube skin

handSolid :: HasMaterials m => m (Solid Material)
handSolid = do
  mats <- askMaterials
  let Just skin = lookupGlobalMaterial (materialId skinMaterial) mats
  pure $ Scaled (Vector3 0.4 1.5 0.4) (cube skin)
    `Subtract`
    (Translated (Vector3 (-0.1) 1.2 0) $ Rotated (quaternionFromEuler 0 0 45) $ Scaled (Vector3 1.5 1 1) $ cube skin)

legSolid :: HasMaterials m => m (Solid Material)
legSolid = do
  mats <- askMaterials
  let Just skin = lookupGlobalMaterial (materialId skinMaterial) mats
  pure $ Scaled (Vector3 0.45 1.5 0.45) $ cube skin

boobsSolid :: HasMaterials m => m (Solid Material)
boobsSolid = do
  mats <- askMaterials
  let Just skin = lookupGlobalMaterial (materialId skinMaterial) mats
  pure $ Scaled (Vector3 0.5 0.5 0.5) $
    (Rotated (quaternionFromEuler 0 0 90) $ Icosahedron skin)
    `Subtract`
    (Translated (Vector3 0 0 0.5) $ Scaled (Vector3 1 1 1) $ cube skin)

-- | Contains all objects inside the human model
data HumanModel = HumanModel {
  humanModelBody      :: Ptr Node
, humanModelHead      :: (Ptr Node, Vector3) -- ^ Head with joint point relative to head
, humanModelRightHand :: (Ptr Node, Vector3)
, humanModelLeftHand  :: (Ptr Node, Vector3)
, humanModelRightLeg  :: (Ptr Node, Vector3)
, humanModelLeftLeg   :: (Ptr Node, Vector3)
}

-- | Create human model
humanModel :: (MonadIO m, HasMaterials m, HasScene m, HasContext m, HasResourceCache m, MonadError UrhoError m)
  => HumanModelConfig -> Ptr Node -> m HumanModel
humanModel cfg pnode = do
  bodyNode <- solidCustomModel pnode =<< bodySolid cfg
  nodeSetPosition bodyNode $ Vector3 0 3 0
  headNode <- solidCustomModel bodyNode =<< headSolid
  nodeSetPosition headNode $ Vector3 0 1.1 0
  rightHand <- solidCustomModel bodyNode =<< handSolid
  nodeSetPosition rightHand $ Vector3 (-0.7) 0 0
  leftHand <- solidCustomModel bodyNode . Rotated (quaternionFromEuler 0 180 0) =<< handSolid
  nodeSetPosition leftHand $ Vector3 (0.7) 0 0
  rightLeg <- solidCustomModel bodyNode =<< legSolid
  nodeSetPosition rightLeg $ Vector3 (-0.25) (-1.5) (0.025)
  leftLeg <- solidCustomModel bodyNode =<< legSolid
  nodeSetPosition leftLeg $ Vector3 (0.25) (-1.5) (0.025)
  pure HumanModel {
      humanModelBody = bodyNode
    , humanModelHead = (headNode, Vector3 0 (-0.35) 0)
    , humanModelRightHand = (rightHand, Vector3 (0.12) 0.6 0)
    , humanModelLeftHand = (leftHand, Vector3 (-0.12) 0.6 0)
    , humanModelRightLeg = (rightLeg, Vector3 0 0.75 0)
    , humanModelLeftLeg = (leftLeg, Vector3 0 0.75 0)
    }
