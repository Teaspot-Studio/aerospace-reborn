module Aerospace.Client.Camera.Basic(
    cameraComponent
  ) where

import Aerospace.Client.Camera.Class
import Aerospace.Client.Env.Reactive
import Aerospace.Client.Input
import Aerospace.Client.Monad
import Aerospace.Client.UI

import qualified Data.Map.Strict as M
import qualified Data.Vector as V


-- | Create new camera and setup updates for it. Note that you never will get event if camera creation
-- is failed.
cameraComponent :: ReactiveMonad t m => CameraConfig t -> m (Event t (CameraComponent t))
cameraComponent CameraConfig{..} = do
  buildE <- getPostBuild
  mcamE <- performMainThread $ ffor buildE $ const $ do
    cameraNode <- nodeCreateChild cameraCfgParent "Camera" CMLocal 0
    mcam <- nodeCreateComponent cameraNode Nothing Nothing
    whenNothing mcam $ liftIO $ putStrLn "Cannot create camera node!"
    pure (mcam, cameraNode)
  let camE = fforMaybe mcamE $ \case
        (Just camPtr, camNode) -> Just (camPtr, camNode)
        _ -> Nothing
  networkWith_ camE $ \(_, camNode) -> do
    updAnglesE <- getUpdatedWithInit cameraCfgAngles
    performMainThread_ $ ffor updAnglesE $ \(Vector3 pa ya ra) -> nodeSetRotation camNode $ quaternionFromEuler pa ya ra
    updEyeE <- getUpdatedWithInit cameraCfgEye
    performMainThread_ $ ffor updEyeE $ nodeSetPosition camNode
  eyeVel <- differentiateWithReset cameraCfgEyeVel cameraCfgEye
  pure $ ffor camE $ \(camPtr, camNode) -> CameraComponent {
      cameraAngles = cameraCfgAngles
    , cameraEye = cameraCfgEye
    , cameraEyeVel = eyeVel
    , cameraDebugGeometry = cameraCfgDebugGeometry
    , cameraNode = camNode
    , cameraPtr = camPtr
    }
