module Aerospace.Client.Camera.Fly(
    flyCamera
  ) where

import Aerospace.Client.Camera.Class
import Aerospace.Client.Camera.Basic
import Aerospace.Client.Env.Reactive
import Aerospace.Client.Input
import Aerospace.Client.Monad
import Aerospace.Client.UI

import qualified Data.Map.Strict as M
import qualified Data.Vector as V

-- | Make camera that can fly around with wasd and turn around with mouse
flyCamera :: ReactiveMonad t m => CameraConfig t -> m (Event t (CameraComponent t))
flyCamera cfg = do
  cursorVisbleD <- getCursorVisible
  focusD <- getUIFocused
  updE <- getUpdateEvent
  dtD <- engineUpdateStep
  mouseD <- inputMouseMove
  -- Calculate view angles
  let
    calcAngles (That newVal) _ = pure newVal
    calcAngles (These _ newVal) _ = pure newVal
    calcAngles _ oldAngles = do
      melem <- sample . current $ focusD
      case melem of
        Just _ -> pure oldAngles
        Nothing -> do
          -- Mouse sensitivity as degrees per pixel
          let mouseSensitivity = 0.1
          -- Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
          cursorVisible <- sample . current $ cursorVisbleD
          if not cursorVisible then do
              mouse <- sample . current $ mouseD
              let pitch = clamp (-90) 90 $ oldAngles ^. x + mouseSensitivity * fromIntegral (mouse ^. y)
              let yaw = oldAngles ^. y + mouseSensitivity * fromIntegral (mouse ^. x)
              pure $ Vector3 pitch yaw (oldAngles ^. z)
            else pure oldAngles
  initAngles <- sample . current $ cameraCfgAngles cfg
  anglesD <- foldDynM calcAngles initAngles $ align updE (updated $ cameraCfgAngles cfg)
  -- Calculate movement
  forwardD <- inputKeyIsDown ScancodeW
  backwardD <- inputKeyIsDown ScancodeS
  leftD <- inputKeyIsDown ScancodeA
  rightD <- inputKeyIsDown ScancodeD
  let
    calcMovement (That newVal) _ = pure newVal
    calcMovement (These _ newVal) _ = pure newVal
    calcMovement (This _) oldPos = do
      melem <- sample . current $ focusD
      case melem of
        Just _ -> pure oldPos
        Nothing -> do
          -- Movement speed as world units per second
          let moveSpeed = 20
              mul (Vector3 a b c) v = Vector3 (a*v) (b*v) (c*v)
          dt <- sample . current $ dtD
          Vector3 pa ya ra <- sample . current $ anglesD
          let
            q = quaternionFromEuler pa ya ra
            forwardVec = quaternionRotate q vec3Forward
            backVec = quaternionRotate q vec3Back
            leftVec = quaternionRotate q vec3Left
            rightVec = quaternionRotate q vec3Right
            addV с v a = if с then a + (v `mul` (moveSpeed * dt)) else a
          forwardPressed <- sample . current $ forwardD
          backwardPressed <- sample . current $ backwardD
          leftPressed <- sample . current $ leftD
          rightPressed <- sample . current $ rightD
          pure
            $ addV forwardPressed forwardVec
            $ addV backwardPressed backVec
            $ addV leftPressed leftVec
            $ addV rightPressed rightVec
            $ oldPos
  initPos <- sample . current $ cameraCfgEye cfg
  eyeD <- foldDynM calcMovement initPos $ align updE (updated $ cameraCfgEye cfg)
  let eyeVelE = fmap (const 0) . ffilter not . updated $ do
        forwardPressed <- forwardD
        backwardPressed <- backwardD
        leftPressed <- leftD
        rightPressed <- rightD
        pure $ forwardPressed || backwardPressed || leftPressed || rightPressed
  let eyeVelResetE = leftmost [updated $ cameraCfgEyeVel cfg, eyeVelE]
  eyeVelD <- holdDyn 0 eyeVelResetE
  -- Calculate debug geometry
  let
    calcDebugGeometry (That newVal) _ = newVal
    calcDebugGeometry (These _ newVal) _ = newVal
    calcDebugGeometry _ oldVal = not oldVal
  dbgToggleE <- inputKeyPress ScancodeSpace
  initDbgGeometry <- sample . current $ cameraCfgDebugGeometry cfg
  dbgGeometryD <- foldDyn calcDebugGeometry initDbgGeometry $ align dbgToggleE (updated $ cameraCfgDebugGeometry cfg)
  let
    newCfg = cfg {
        cameraCfgAngles = anglesD
      , cameraCfgEye = eyeD
      , cameraCfgEyeVel = eyeVelD
      , cameraCfgDebugGeometry = dbgGeometryD
      }
  resCamE <- cameraComponent newCfg
  pure resCamE
