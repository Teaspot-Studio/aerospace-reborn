module Aerospace.Client.Camera.Class(
    CameraConfig(..)
  , CameraComponent(..)
  ) where

import Aerospace.Client.Env.Reactive
import Aerospace.Client.Input
import Aerospace.Client.Monad
import Aerospace.Client.UI

import qualified Data.Map.Strict as M
import qualified Data.Vector as V

-- | Inputs for camera
data CameraConfig t = CameraConfig {
  cameraCfgParent        :: !(Ptr Node)
, cameraCfgAngles        :: !(Dynamic t Vector3) -- ^ Eye euler angles relative to the parent
, cameraCfgEye           :: !(Dynamic t Vector3) -- ^ Eye position relative to the parent
, cameraCfgEyeVel        :: !(Dynamic t Vector3) -- ^ Eye manual velocity relative to the parent (used for prediction)
, cameraCfgDebugGeometry :: !(Dynamic t Bool) -- ^ Whether debug geometry is enabled
}

-- | Runtime camera data
data CameraComponent t = CameraComponent {
  cameraAngles        :: !(Dynamic t Vector3) -- ^ Eye euler angles relative to the parent
, cameraEye           :: !(Dynamic t Vector3) -- ^ Eye position relative to the parent
, cameraEyeVel        :: !(Dynamic t Vector3) -- ^ Eye current velocity relative to the parent
, cameraDebugGeometry :: !(Dynamic t Bool) -- ^ Whether debug geometry is enabled
, cameraNode          :: !(Ptr Node) -- ^ The node that contains camera
, cameraPtr           :: !(Ptr Camera) -- ^ Pointer to camera
}
