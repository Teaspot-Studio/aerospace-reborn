module Aerospace.Client.Env(
    Env(..)
  , createEnv
  , runEnvApplication
  ) where

import Aerospace.Client.Config
import Aerospace.Client.Model.Human
import Aerospace.Client.Options
import Aerospace.Material
import Aerospace.Urho
import Aerospace.Util
import Control.Concurrent
import Control.Monad.Except
import Data.IORef
import Data.StateVar
import Foreign hiding (void)
import Game.GoreAndAsh.Core.ExternalRef
import Game.GoreAndAsh.Core.Monad
import Graphics.Urho3D hiding (HasContext, Material, whenJust)
import Reflex.Spider
import System.Directory (canonicalizePath)
import System.Environment (setEnv)

import qualified Data.Text as T
import qualified Graphics.Urho3D as Urho

-- | Non reactive environment of application. Used before we setup reactive environment.
data Env = Env {
  envContext            :: !(Ptr Context)
, envApplication        :: SharedPtr Application
, envResourceCache      :: Ptr ResourceCache
, envMaterials          :: !MaterialMap
, envOptions            :: !Options
, envConfig             :: !Config
, envScene              :: !(IORef (Maybe (SharedPtr Scene)))
, envCursor             :: !(IORef (Maybe (SharedPtr Cursor)))
, envUpdateCallback     :: !(IORef (EventUpdate -> IO ()))
, envPostRenderCallback :: !(IORef (EventPostRenderUpdate -> IO ()))
, envKeyDownCallback    :: !(IORef (EventKeyDown -> IO ()))
, envKeyUpCallback      :: !(IORef (EventKeyUp -> IO ()))
, envInitialized        :: !(MVar ()) -- ^ Fired when all subsystem initialized and it is safe to init reactive environment
}

instance Monad m => HasContext (ReaderT Env m) where
  getUrhoContext = asks envContext
  {-# INLINE getUrhoContext #-}

instance Monad m => HasResourceCache (ReaderT Env m) where
  getResourceCache = asks envResourceCache
  {-# INLINE getResourceCache #-}

instance Monad m => HasMaterials (ReaderT Env m) where
  askMaterials = asks envMaterials
  {-# INLINE askMaterials #-}

-- | Initialize environment for the given scope
createEnv :: MonadIO m => Options -> m Env
createEnv opts@Options{..} = do
  liftIO $ do
    p' <- canonicalizePath optionsResourcesPath
    setEnv "URHO3D_PREFIX_PATH" p'
  cntx <- newObject ()
  cfg <- loadConfig optionsConfigPath
  sceneRef <- liftIO $ newIORef Nothing
  cursorRef <- liftIO $ newIORef Nothing
  initedRef <- liftIO newEmptyMVar
  updRef <- liftIO $ newIORef (const $ pure ())
  prenderRef <- liftIO $ newIORef (const $ pure ())
  keyDownRef <- liftIO $ newIORef (const $ pure ())
  keyUpRef <- liftIO $ newIORef (const $ pure ())
  let env = Env {
          envContext = cntx
        , envApplication = error "envApplication triggered too early!"
        , envResourceCache = error "envResourceCache triggered too early!"
        , envMaterials = coreMaterials
        , envOptions = opts
        , envConfig = cfg
        , envScene = sceneRef
        , envCursor = cursorRef
        , envUpdateCallback = updRef
        , envPostRenderCallback = prenderRef
        , envKeyDownCallback = keyDownRef
        , envKeyUpCallback = keyUpRef
        , envInitialized = initedRef
        }
      liftEnv f app = do
        cache :: Ptr ResourceCache <- guardNothing "ResourceCache" =<< getSubsystem app
        runReaderT f env {
            envApplication   = makePointer app
          , envResourceCache = cache
          }
  app <- newSharedObject ApplicationCreate {
      appCreateContext = cntx
    , appCreateSetup = liftEnv setupCallback
    , appCreateStart = liftEnv startCallback
    , appCreateStop = liftEnv stopCallback
    }
  let

  pure env { envApplication = app }
  where
    setupCallback :: ReaderT Env IO ()
    setupCallback = do
      app <- asks envApplication
      startupParameter app "WindowTitle" $= ("Aerospace" :: T.Text)
      startupParameter app "LogName" $= optionsLogPath
      startupParameter app "ForceGL2" $= False
      startupParameter app "Sound" $= True
      applyConfigSetup

    startCallback :: ReaderT Env IO ()
    startCallback = do
      app <- asks envApplication
      cache :: Ptr ResourceCache <- guardNothing "ResourceCache" =<< getSubsystem app
      graphics :: Ptr Graphics <- guardNothing "Graphics" =<< getSubsystem app
      iconM <- cacheGetResource cache "Textures/Icon.png" True
      _ <- whenJust iconM $ graphicsSetWindowIcon graphics
      mscene <- runExceptT createScene
      scene <- case mscene of
        Left er -> fail $ T.unpack $ renderUrhoError er -- TODO: better error processing
        Right a -> pure a
      sceneRef <- asks envScene
      liftIO $ writeIORef sceneRef $ Just scene
      cursor <- createUI
      cursorRef <- asks envCursor
      liftIO $ writeIORef cursorRef $ Just cursor
      initMouseMode MM'Relative
      subscribeToEvents
      initedRef <- asks envInitialized
      liftIO $ putMVar initedRef ()

    stopCallback :: ReaderT Env IO ()
    stopCallback = do
      app <- asks envApplication
      eng <- applicationEngine app
      engineDumpResources eng optionsConfigDump

-- | Execute application main thread
runEnvApplication :: MonadIO m => Env -> m ()
runEnvApplication Env{..} = applicationRun envApplication

-- | Subscribe to engine events and fire our internal events
subscribeToEvents :: (MonadReader Env m, MonadIO m) => m ()
subscribeToEvents = do
  env <- ask
  let app = envApplication env
      liftEnv :: forall a . (a -> ReaderT Env IO ()) -> a -> IO ()
      liftEnv f arg = runReaderT (f arg) env
  subscribeToEvent app $ liftEnv fireUpdateEvents
  subscribeToEvent app $ liftEnv firePostRenderEvents
  subscribeToEvent app $ liftEnv fireKeyDownEvents
  subscribeToEvent app $ liftEnv fireKeyUpEvents

-- | Inform our listeners about engine update
fireUpdateEvents :: (MonadReader Env m, MonadIO m) => EventUpdate -> m ()
fireUpdateEvents e = do
  cbRef <- asks envUpdateCallback
  liftIO $ do
    cb <- readIORef cbRef
    cb e

-- | Inform our listeners about post render event
firePostRenderEvents :: (MonadReader Env m, MonadIO m) => EventPostRenderUpdate -> m ()
firePostRenderEvents e = do
  cbRef <- asks envPostRenderCallback
  liftIO $ do
    cb <- readIORef cbRef
    cb e

-- | Inform our listeners about keyboard key down event
fireKeyDownEvents :: (MonadReader Env m, MonadIO m) => EventKeyDown -> m ()
fireKeyDownEvents e = do
  cbRef <- asks envKeyDownCallback
  liftIO $ do
    cb <- readIORef cbRef
    cb e

-- | Inform our listeners about keyboard key up event
fireKeyUpEvents :: (MonadReader Env m, MonadIO m) => EventKeyUp -> m ()
fireKeyUpEvents e = do
  cbRef <- asks envKeyUpCallback
  liftIO $ do
    cb <- readIORef cbRef
    cb e

-- | Apply startup configurations stored in config
applyConfigSetup :: (MonadReader Env m, MonadIO m) => m ()
applyConfigSetup = do
  app <- asks envApplication
  Config{..} <- asks envConfig
  startupParameter app "FullScreen" $= configFullScreen
  startupParameter app "Headless" $= configFullScreen

-- | Construct the scene content.
createScene :: (MonadReader Env m, MonadIO m, HasContext m, HasMaterials m, HasResourceCache m, MonadError UrhoError m) => m (SharedPtr Scene)
createScene = do
  app <- asks envApplication
  cache :: Ptr ResourceCache <- guardNothing "ResourceCache" =<< getSubsystem app
  cntx <- getContext app
  scene :: SharedPtr Scene <- newSharedObject cntx

  {-
    Create octree, use default volume (-1000, -1000, -1000) to (1000, 1000, 1000)
  -}
  _ :: Ptr Octree <- guardNothing "Failed to create Octree" =<< nodeCreateComponent scene Nothing Nothing

  -- Create a Zone component for ambient lighting & fog control
  zoneNode <- nodeCreateChild scene "Zone" CMLocal 0
  zone :: Ptr Zone <- guardNothing "Failed to create Zone" =<< nodeCreateComponent zoneNode Nothing Nothing
  -- Set same volume as the Octree, set a close bluish fog and some ambient light
  zoneSetBoundingBox zone $ BoundingBox (-1000) 1000
  zoneSetAmbientColor zone $ rgb 0.15 0.15 0.15
  zoneSetFogColor zone $ rgb 0.2 0.2 0.2
  zoneSetFogStart zone 200
  zoneSetFogEnd zone 300

  -- Create a directional light
  lightNode <- nodeCreateChild scene "DirectionalLight" CMLocal 0
  nodeSetDirection lightNode (Vector3 0.5 (-1.0) 0.1) -- The direction vector does not need to be normalized
  light :: Ptr Light <- guardNothing "Failed to create Light" =<< nodeCreateComponent lightNode Nothing Nothing
  lightSetLightType light LT'Directional
  drawableSetCastShadows light True
  lightSetShadowBias light $ BiasParameters 0.00025 0.5
  -- Set cascade splits at 10, 50 and 200 world units, fade shadows out at 80% of maximum shadow distance
  lightSetShadowCascade light $ CascadeParameters 10 50 200 0 0.8 1.0

  {-
   Create a child scene node (at world origin) and a StaticModel component into it. Set the StaticModel to show a simple
   plane mesh with a "stone" material. Note that naming the scene nodes is optional. Scale the scene node larger
   (100 x 100 world units)
  -}
  planeNode <- nodeCreateChild scene "Plane" CMReplicated 0
  nodeSetScale planeNode (Vector3 100 1 100)
  (planeObject :: Ptr StaticModel) <- guardNothing "Plane StaticModel" =<< nodeCreateComponent planeNode Nothing Nothing
  (planeModel :: Ptr Model) <- guardNothing "Plane.mdl" =<< cacheGetResource cache "Models/Plane.mdl" True
  staticModelSetModel planeObject planeModel
  (planeMaterial :: Ptr Urho.Material) <- guardNothing "StoneTiled.xml" =<< cacheGetResource cache "Materials/StoneTiled.xml" True
  staticModelSetMaterial planeObject planeMaterial

  return scene

-- | Create default UI
createUI :: (MonadReader Env m, MonadIO m) => m (SharedPtr Cursor)
createUI = do
  app <- asks envApplication
  cache :: Ptr ResourceCache <- guardNothing "ResourceCache" =<< getSubsystem app
  ui :: Ptr UI <- guardNothing "UI" =<< getSubsystem app
  cntx <- getContext app

  -- Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
  -- control the camera, and when visible, it will point the raycast target
  style :: Ptr XMLFile <- guardNothing "DefaultStyle.xml" =<< cacheGetResource cache "UI/DefaultStyle.xml" True
  cursor :: SharedPtr Cursor <- newSharedObject cntx
  _ <- uiElementSetStyleAuto cursor style
  uiSetCursor ui $ pointer cursor
  _ <- uiElementSetVisible cursor False
  pure cursor

-- | Change mouse visibility and behavior
initMouseMode :: (MonadReader Env m, MonadIO m) => MouseMode -> m ()
initMouseMode mode = do
  app <- asks envApplication
  input :: Ptr Input <- guardNothing "Input" =<< getSubsystem app
  when (mode == MM'Free) $ inputSetMouseVisible input True False
  when (mode /= MM'Absolute) $ inputSetMouseMode input mode False
