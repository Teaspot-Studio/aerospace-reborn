module Aerospace.Client.Env.Reactive(
  -- * Subsystems queries
    getApplication
  , getUI
  , getInput
  , getGraphics
  , getResourceCache
  , getRenderer
  , getOctree
  , getScene
  , getCursor
  -- * Core events
  , getUpdateEvent
  , getPostRenderEvent
  , engineUpdateStep
  , getKeyUpEvent
  , getKeyDownEvent
  -- * Cached events and dynamics
  , storeCacheEvent
  , storeCacheDynamic
  , deleteCacheEvent
  , deleteCacheDynamic
  , cachedEvent
  , cachedDynamic
  , autoCachedEvent
  , autoCachedDynamic
  -- * Multithreading utilities
  , performMainThread
  , performMainThread_
  -- * Other reactive utitlities
  , getUpdatedWithInit
  , networkWith_
  , dbgPrintE
  -- * Reexports
  , module Aerospace.Client.Env.Reactive.Class
  ) where

import Aerospace.Client.Config
import Aerospace.Client.Entity.Class
import Aerospace.Client.Env
import Aerospace.Client.Env.Reactive.Class
import Aerospace.Client.Options
import Aerospace.Reactive
import Aerospace.Util
import Control.Concurrent
import Control.Lens ((^.))
import Control.Monad
import Control.Monad.Reader
import Data.Align
import Data.Bifunctor
import Data.Foldable (traverse_)
import Data.IORef
import Data.Map.Strict (Map)
import Data.Maybe
import Data.Monoid
import Data.These
import Foreign hiding (void)
import Game.GoreAndAsh
import Game.GoreAndAsh.Core.ExternalRef
import Game.GoreAndAsh.Core.Monad
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Network.Backend.TCP
import Game.GoreAndAsh.Sync
import GHC.Generics
import Graphics.Urho3D hiding (Event, constant, scale)
import Graphics.Urho3D.Multithread
import Reflex
import Reflex.Spider

import qualified Data.Dynamic as D
import qualified Data.Map.Strict as M

-- | Query application pointer from environment
getApplication :: MonadReader (REnv t) m => m (SharedPtr Application)
getApplication = asks renvApplication

-- | Query UI subystem
getUI :: MonadReader (REnv t) m => m (Ptr UI)
getUI = asks renvUI

-- | Query Input subystem
getInput :: MonadReader (REnv t) m => m (Ptr Input)
getInput = asks renvInput

-- | Query Graphics subystem
getGraphics :: MonadReader (REnv t) m => m (Ptr Graphics)
getGraphics = asks renvGraphics

-- | Query resource subystem
getResourceCache :: MonadReader (REnv t) m => m (Ptr ResourceCache)
getResourceCache = asks renvResourceCache

-- | Query renderer subsystem
getRenderer :: MonadReader (REnv t) m => m (Ptr Renderer)
getRenderer = asks renvRenderer

-- | Query octree subystem of scene
getOctree :: MonadReader (REnv t) m => m (Ptr Octree)
getOctree = asks renvOctree

-- | Query octree subystem of scene
getScene :: MonadReader (REnv t) m => m (SharedPtr Scene)
getScene = asks renvScene

-- | Query octree object of UI
getCursor :: MonadReader (REnv t) m => m (SharedPtr Cursor)
getCursor = asks renvCursor

-- | Query event that fires when engine updates
getUpdateEvent :: MonadReader (REnv t) m => m (Event t EventUpdate)
getUpdateEvent = asks renvUpdateEvent

-- | Query event that fires when frame is renderered
getPostRenderEvent :: MonadReader (REnv t) m => m (Event t EventPostRenderUpdate)
getPostRenderEvent = asks renvPostRenderEvent

-- | Get last update time relative to the previous update in seconds
engineUpdateStep :: (MonadReader (REnv t) m, MonadIO m, MonadHold t m, D.Typeable t, Reflex t) => m (Dynamic t Float)
engineUpdateStep = autoCachedDynamic "engineUpdateStep" $ do
  updE <- getUpdateEvent
  holdDyn 0 $ _eventUpdateTimeStep <$> updE

-- | Get event that fires when user press keyboard button
getKeyDownEvent :: MonadReader (REnv t) m => m (Event t EventKeyDown)
getKeyDownEvent = asks renvKeyDown

-- | Get event that fires when user unpress  keyboard button
getKeyUpEvent :: MonadReader (REnv t) m => m (Event t EventKeyUp)
getKeyUpEvent = asks renvKeyUp

-- | Execute action in main thread and wait for result
performMainThread :: (TriggerEvent t m, PerformEvent t m, MonadIO (Performable m), MonadReader (REnv t) m)
  => Event t (ReaderT (REnv t) IO a) -> m (Event t a)
performMainThread maE = do
  env <- ask
  performEventAsync $ ffor maE $ \ma ret -> void . liftIO . forkIO $ do
    resVar <- newEmptyMVar
    runInMainThread $ putMVar resVar =<< runReaderT ma env
    ret =<< takeMVar resVar

-- | Execute action in main thread and don't wait for result
performMainThread_ :: (TriggerEvent t m, PerformEvent t m, MonadIO (Performable m), MonadReader (REnv t) m)
  => Event t (ReaderT (REnv t) IO a) -> m ()
performMainThread_ maE = do
  env <- ask
  performEvent_ $ ffor maE $ void . liftIO . forkIO . runInMainThread . void . flip runReaderT env  

-- | Same as 'updated' but returns also initial value at post build
getUpdatedWithInit :: PostBuild t m => Dynamic t a -> m (Event t a)
getUpdatedWithInit d = do
  buildE <- getPostBuild
  let initE = tag (current d) buildE
  pure $ leftmost [updated d, initE]

-- | Waith for event and switch FRP network into action
networkWith_ :: (Adjustable t m, MonadHold t m) => Event t a -> (a -> m ()) -> m ()
networkWith_ e f = void $ networkHold (pure ()) $ f <$> e

-- | Print occurences of event in place
dbgPrintE :: (PerformEvent t m, MonadIO (Performable m), Show a) => Event t a -> m ()
dbgPrintE = performEvent_ . fmap (liftIO . print)

-- | Store event in cache. Use this for expensive global events that are not wanted to be
-- replicated.
storeCacheEvent :: (MonadReader (REnv t) m, MonadIO m, D.Typeable t, D.Typeable a) => EventName -> Event t a -> m ()
storeCacheEvent ename e = do
  mref <- asks renvCachedEvents
  liftIO $ atomicModifyIORef' mref $ (, ()) . M.insert ename (D.toDyn e)

-- | Store event in cache. Use this for expensive global events that are not wanted to be
-- replicated.
storeCacheDynamic :: (MonadReader (REnv t) m, MonadIO m, D.Typeable t, D.Typeable a) => DynamicName -> Dynamic t a -> m ()
storeCacheDynamic ename e = do
  mref <- asks renvCachedDynamics
  liftIO $ atomicModifyIORef' mref $ (, ()) . M.insert ename (D.toDyn e)

-- | Removes event from cache. See 'storeCacheEvent'
deleteCacheEvent :: (MonadReader (REnv t) m, MonadIO m) => EventName -> m ()
deleteCacheEvent ename = do
  mref <- asks renvCachedEvents
  liftIO $ atomicModifyIORef' mref $ (, ()) . M.delete ename

-- | Removes dynamic from cache. See 'storeCacheDynamic'
deleteCacheDynamic :: (MonadReader (REnv t) m, MonadIO m) => DynamicName -> m ()
deleteCacheDynamic ename = do
  mref <- asks renvCachedDynamics
  liftIO $ atomicModifyIORef' mref $ (, ()) . M.delete ename

-- | Query event from cache
cachedEvent :: (MonadReader (REnv t) m, MonadIO m, D.Typeable t, D.Typeable a) => EventName -> m (Maybe (Event t a))
cachedEvent ename = do
  mref <- asks renvCachedDynamics
  m <- liftIO $ readIORef mref
  pure $ D.fromDynamic =<< M.lookup ename m

-- | Query dynamic from cache
cachedDynamic :: (MonadReader (REnv t) m, MonadIO m, D.Typeable t, D.Typeable a) => DynamicName -> m (Maybe (Dynamic t a))
cachedDynamic ename = do
  mref <- asks renvCachedDynamics
  m <- liftIO $ readIORef mref
  pure $ D.fromDynamic =<< M.lookup ename m

-- | Stores event in cache and in subsequent calls, queries it from cache. Use this for expensive
-- global events that are not wanted to be replicated.
autoCachedEvent :: (MonadReader (REnv t) m, MonadIO m, D.Typeable t, D.Typeable a) => EventName -> m (Event t a) -> m (Event t a)
autoCachedEvent ename ma = do
  me <- cachedEvent ename
  case me of
    Nothing -> do
      e <- ma
      storeCacheEvent ename e
      pure e
    Just e -> pure e

-- | Stores dynamic in cache and in subsequent calls, queries it from cache. Use this for expensive
-- global dynamics that are not wanted to be replicated.
autoCachedDynamic :: (MonadReader (REnv t) m, MonadIO m, D.Typeable t, D.Typeable a) => DynamicName -> m (Dynamic t a) -> m (Dynamic t a)
autoCachedDynamic ename ma = do
  me <- cachedDynamic ename
  case me of
    Nothing -> do
      e <- ma
      storeCacheDynamic ename e
      pure e
    Just e -> pure e
