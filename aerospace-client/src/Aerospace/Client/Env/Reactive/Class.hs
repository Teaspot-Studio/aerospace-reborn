module Aerospace.Client.Env.Reactive.Class(
    EventName
  , DynamicName
  , REnv(..)
  , ReactiveMonadCore
  , ReactiveMonad
  -- * Reexports
  , module Aerospace.Reactive
  , module Aerospace.Util
  , module Control.Monad
  , module Control.Monad.Reader
  , module Control.Lens
  , module Data.Align
  , module Data.Bifunctor
  , module Data.Foldable
  , module Data.IORef
  , module Data.Map.Strict
  , module Data.Maybe
  , module Data.Monoid
  , module Data.These
  , module Foreign
  , module Game.GoreAndAsh
  , module Game.GoreAndAsh.Logging
  , module Game.GoreAndAsh.Network
  , module Game.GoreAndAsh.Sync
  , module Game.GoreAndAsh.Time
  , module GHC.Generics
  , module Graphics.Urho3D
  , module Reflex
  ) where

import Aerospace.Client.Config
import Aerospace.Client.Entity.Class
import Aerospace.Client.Options
import Aerospace.Material
import Aerospace.Network.Backend.Urho3D
import Aerospace.Reactive
import Aerospace.Urho
import Aerospace.Util
import Control.Lens ((^.))
import Control.Monad
import Control.Monad.Reader
import Data.Align
import Data.Bifunctor
import Data.Foldable
import Data.IORef
import Data.Map.Strict (Map)
import Data.Maybe
import Data.Monoid
import Data.These
import Foreign hiding (void)
import Game.GoreAndAsh
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Sync
import Game.GoreAndAsh.Time
import GHC.Generics
import Graphics.Urho3D hiding (Event, constant, scale, whenJust, HasContext)
import Reflex

import qualified Data.Dynamic as D

-- | Unqiue name of event
type EventName = String
-- | Unqiue name of dynamic
type DynamicName = String

-- | Reactive environment for client.
data REnv t = REnv {
  renvContext         :: !(Ptr Context)
, renvApplication     :: !(SharedPtr Application)
, renvOptions         :: !Options
, renvConfig          :: !(ExternalRef t Config)
, renvMaterials       :: !MaterialMap
, renvScene           :: !(SharedPtr Scene)
, renvCursor          :: !(SharedPtr Cursor)
, renvUI              :: !(Ptr UI)
, renvInput           :: !(Ptr Input)
, renvGraphics        :: !(Ptr Graphics)
, renvResourceCache   :: !(Ptr ResourceCache)
, renvOctree          :: !(Ptr Octree)
, renvRenderer        :: !(Ptr Renderer)
, renvUpdateEvent     :: !(Event t EventUpdate)
, renvPostRenderEvent :: !(Event t EventPostRenderUpdate)
, renvKeyDown         :: !(Event t EventKeyDown)
, renvKeyUp           :: !(Event t EventKeyUp)
, renvCachedEvents    :: !(IORef (Map EventName D.Dynamic))
, renvCachedDynamics  :: !(IORef (Map DynamicName D.Dynamic))
, renvEntityCollEnv   :: !(EntityCollectionEnv t)
}

-- | Shortcut for game functions that polymorphic around 't'
type ReactiveMonadCore t m = (
    Reflex t
  , MonadIO m
  , MonadFix m
  , MonadHold t m
  , MonadSample t m
  , Adjustable t m
  , PostBuild t m
  , MonadIO (Performable m)
  , PerformEvent t m
  , TriggerEvent t m
  , D.Typeable t
  , LoggingMonad t m
  , NetworkMonad t UrhoNetworkBackend m
  , NetworkClient t UrhoNetworkBackend m
  , SyncMonad t UrhoNetworkBackend m )

-- | Shortcut for game functions that polymorphic around 't' with additional environment
type ReactiveMonad t m = (MonadReader (REnv t) m, ReactiveMonadCore t m)

instance {-# OVERLAPPING #-} Monad m => HasContext (ReaderT (REnv t) m) where
  getUrhoContext = asks renvContext
  {-# INLINE getUrhoContext #-}

instance {-# OVERLAPPING #-} Monad m => HasResourceCache (ReaderT (REnv t) m) where
  getResourceCache = asks renvResourceCache
  {-# INLINE getResourceCache #-}

instance {-# OVERLAPPING #-} Monad m => HasScene (ReaderT (REnv t) m) where
  getScene = asks renvScene
  {-# INLINE getScene #-}

instance Monad m => HasMaterials (ReaderT (REnv t) m) where
  askMaterials = asks renvMaterials
  {-# INLINE askMaterials #-}
