module Aerospace.Client.Env.Reactive.Impl(
    newReactiveEnv
  ) where

import Aerospace.Client.Config
import Aerospace.Client.Entity
import Aerospace.Client.Env
import Aerospace.Client.Env.Reactive
import Aerospace.Client.Env.Reactive.Class
import Aerospace.Client.Options
import Control.Concurrent

import qualified Data.Dynamic as D
import qualified Data.Map.Strict as M

-- | Construct reactive environment from nonreactive.
--
-- Note: the function must be called after scene and ui was initialized
newReactiveEnv :: ReactiveMonadCore t m => Env -> m (REnv t)
newReactiveEnv env@Env{..} = do
  cfgRef <- newExternalRef envConfig
  mscene <- liftIO $ readIORef envScene
  ui :: Ptr UI <- guardNothing "UI" =<< getSubsystem envApplication
  input :: Ptr Input <- guardNothing "Input" =<< getSubsystem envApplication
  graphics :: Ptr Graphics <- guardNothing "Graphics" =<< getSubsystem envApplication
  cache :: Ptr ResourceCache <- guardNothing "ResourceCache" =<< getSubsystem envApplication
  renderer :: Ptr Renderer <- guardNothing "Renderer" =<< getSubsystem envApplication
  case mscene of
    Nothing -> fail "newReactiveEnv is called before scene is created!"
    Just scene -> do
      octree :: Ptr Octree <- guardNothing "Octree" =<< nodeGetComponent scene False
      mcursor <- liftIO $ readIORef envCursor
      case mcursor of
        Nothing -> fail "newReactiveEnv is called before cursor is created!"
        Just cursor -> do
          updE <- hookUpdateEvent env
          prenderE <- hookPostRenderEvent env
          keyDownE <- hookKeyDownEvent env
          keyUpE <- hookKeyUpEvent env
          cachedEvents <- liftIO $ newIORef mempty
          cachedDynamics <- liftIO $ newIORef mempty
          rec
            ecollEnv <- newEntityCollectionEnv renv
            let renv = REnv {
                    renvContext = envContext
                  , renvApplication = envApplication
                  , renvOptions = envOptions
                  , renvConfig = cfgRef
                  , renvMaterials = envMaterials
                  , renvScene = scene
                  , renvCursor = cursor
                  , renvUI = ui
                  , renvInput = input
                  , renvGraphics = graphics
                  , renvResourceCache = cache
                  , renvOctree = octree
                  , renvRenderer = renderer
                  , renvUpdateEvent = updE
                  , renvPostRenderEvent = prenderE
                  , renvKeyDown = keyDownE
                  , renvKeyUp = keyUpE
                  , renvCachedEvents = cachedEvents
                  , renvCachedDynamics = cachedDynamics
                  , renvEntityCollEnv = ecollEnv
                  }
          pure renv

-- | Place reactive callback on engine update
hookUpdateEvent :: (TriggerEvent t m, MonadIO m) => Env -> m (Event t EventUpdate)
hookUpdateEvent Env{..} = do
  (e, fire) <- newTriggerEvent
  liftIO $ writeIORef envUpdateCallback fire
  pure e

-- | Place reactive callback on engine post render update
hookPostRenderEvent :: (TriggerEvent t m, MonadIO m) => Env -> m (Event t EventPostRenderUpdate)
hookPostRenderEvent Env{..} = do
  (e, fire) <- newTriggerEvent
  liftIO $ writeIORef envPostRenderCallback fire
  pure e

-- | Place reactive callback on engine keyboard key down event
hookKeyDownEvent :: (TriggerEvent t m, MonadIO m) => Env -> m (Event t EventKeyDown)
hookKeyDownEvent Env{..} = do
  (e, fire) <- newTriggerEvent
  liftIO $ writeIORef envKeyDownCallback fire
  pure e

-- | Place reactive callback on engine keyboard key up event
hookKeyUpEvent :: (TriggerEvent t m, MonadIO m) => Env -> m (Event t EventKeyUp)
hookKeyUpEvent Env{..} = do
  (e, fire) <- newTriggerEvent
  liftIO $ writeIORef envKeyUpCallback fire
  pure e

instance ReactiveMonadCore t m => EntityCollectionMonad t (ReaderT (REnv t) m) where
  waitEntity e = do
    env <- ask
    waitEntityImpl (renvEntityCollEnv env) e
  {-# INLINE waitEntity #-}
