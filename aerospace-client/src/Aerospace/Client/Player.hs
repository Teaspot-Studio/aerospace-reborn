{-# LANGUAGE NoMonomorphismRestriction #-}
module Aerospace.Client.Player(
    ClientPlayer(..)
  , playerComponent
  ) where

import Aerospace.Client.Camera
import Aerospace.Client.Entity
import Aerospace.Client.Env.Reactive
import Aerospace.Player
import Data.Proxy

-- | Client side info of player
data ClientPlayer t = ClientPlayer {
-- | Unique id of player
  cplayerId     :: !PlayerId
-- | Entity attached to the player
, cplayerEntity :: !(ClientEntity t)
-- | Camera of player
, cplayerCamera :: !(CameraComponent t)
}

-- | Create client player and start synching and prediction
playerComponent :: forall t m . (ReactiveMonad t m, EntityCollectionMonad t m)
  => m (Event t (ClientPlayer t))
playerComponent = do
  scene <- getScene
  buildE <- getPostBuild
  rec
    tickE <- tickEveryUntil 0.2 selfIdMsg
    _ <- sendToServer playerGlobalId ReliableMessage $ ReqSelfPlayerId <$ leftmost [tickE, buildE]
    msgE <- receiveFromServer playerGlobalId
    selfIdMsg <- headE $ fforMaybe msgE $ \case
          RespSelfPlayerId pid eid -> Just (pid, eid)
          _ -> Nothing
  -- logDebugE $ showl <$> selfIdMsg
  networkHoldE $ ffor selfIdMsg $ \(pid, eid) -> do
    rec
      -- Make Y loop here to feed correction from server to camera
      camCfg <- CameraConfig
        <$> pure (parentPointer scene)
        <*> (holdDyn (playerRotation defPlayer) =<< delay 0.01 (playerRotation corrPlayerE')) -- delay here to break loop
        <*> (holdDyn (playerPosition defPlayer) =<< delay 0.01 (playerPosition corrPlayerE'))
        <*> (holdDyn (playerVelocity defPlayer) =<< delay 0.01 (playerVelocity corrPlayerE'))
        <*> pure (pure False)
      camE <- flyCamera camCfg
      -- logDebugE $ "Camera inited!" <$ camE
      --  When camera is inited, start syncing
      res <- networkHold (pure (never, defPlayerE)) $ ffor camE $ \cam -> do
        waitInitE <- delay 0.01 =<< getPostBuild
        -- logDebugE $ "Start waiting entity!" <$ waitInitE
        -- Fetch client side entity
        ceE <- waitEntity $ eid <$ waitInitE
        -- logDebugE $ "Entity inited!" <$ ceE
        corrPlayerE <- syncPlayerPos pid cam
        performEvent_ $ ffor ceE $ \ce -> nodeRemoveComponent' (centityNode ce) (Proxy :: Proxy StaticModel)
        let resE = ffor ceE $ \ce -> ClientPlayer {
              cplayerId = pid
            , cplayerEntity = ce
            , cplayerCamera = cam
            }
        pure (resE, corrPlayerE)
      let resE' = switch . current $ fst <$> res
          corrPlayerE' = switchPlayer $ snd <$> res
    pure resE'
  where
    defPlayer :: PlayerH
    defPlayer = Player {
        playerPosition = Vector3 2 2 2
      , playerVelocity = Vector3 0 0 0
      , playerRotation = Vector3 0 0 0
      }
    defPlayerE :: PlayerE t
    defPlayerE = Player {
        playerPosition = never
      , playerVelocity = never
      , playerRotation = never
      }

    -- Returns corrected player
    syncPlayerPos :: PlayerId -> CameraComponent t -> m (PlayerE t)
    syncPlayerPos pid cam = fmap switchPlayer $ syncWithName (playerSyncName pid) defPlayerE $ do
      let sync i a = fmap serverRejected $ syncToServer i UnsequencedMessage =<< alignWithFps playerSyncFPS a
      playerPosition <- sync playerPositionId $ cameraEye cam
      playerVelocity <- sync playerVelocityId $ cameraEyeVel cam
      playerRotation <- sync playerRotationId $ cameraAngles cam
      pure Player{..}
