module Aerospace.Client.Input(
    inputMouseMove
  , inputKeyDown
  , inputKeyUp
  , inputKeyIsDown
  , inputKeyPress
  ) where

import Aerospace.Client.Monad
import Aerospace.Client.Env.Reactive
import Aerospace.Util
import Foreign hiding (void)
import Game.GoreAndAsh
import Graphics.Urho3D hiding (Event)

-- | Get global mouse movement from last frame
inputMouseMove :: ReactiveMonad t m => m (Dynamic t IntVector2)
inputMouseMove = autoCachedDynamic "inputMouseMove" $ do
  input <- getInput
  updE <- getUpdateEvent
  moveE <- performMainThread $ ffor updE $ const $ inputGetMouseMove input
  holdDyn (IntVector2 0 0) moveE

-- | Get event that fires when given key is down
inputKeyDown :: ReactiveMonad t m => Scancode -> m (Event t EventKeyDown)
inputKeyDown k = do
  e <- getKeyDownEvent
  pure $ ffilter ((== k) . pressScancode) e

-- | Get event that fires when given key is up
inputKeyUp :: ReactiveMonad t m => Scancode -> m (Event t EventKeyUp)
inputKeyUp k = do
  e <- getKeyUpEvent
  pure $ ffilter ((== k) . upScancode) e

-- | Construct dynamic that catches if key is down
inputKeyIsDown :: ReactiveMonad t m => Scancode -> m (Dynamic t Bool)
inputKeyIsDown k = do
  downE <- getKeyDownEvent
  upE <- getKeyUpEvent
  let downE' = True <$ ffilter ((== k) . pressScancode) downE
      upE' = False <$ ffilter ((== k) . upScancode) upE
  holdDyn False $ leftmost [upE', downE']

-- | Get event that fires when given key is pressed
inputKeyPress :: ReactiveMonad t m => Scancode -> m (Event t ())
inputKeyPress k = autoCachedEvent ("inputKeyPress" ++ show k) $ do
  input <- getInput
  updE <- getUpdateEvent
  resE <- performMainThread $ ffor updE $ const $ inputGetScancodePress input k
  pure $ fforMaybe resE $ \v -> if v then Just () else Nothing
