module Aerospace.Client.Monad(
    InitAppMonad
  , initReadyEvent
  , AppMonad
  , AppPeer
  ) where

import Aerospace.Client.Env
import Aerospace.Client.Env.Reactive
import Aerospace.Client.Env.Reactive.Impl
import Aerospace.Network.Backend.Urho3D
import Control.Concurrent
import Control.Monad
import Control.Monad.Reader
import Game.GoreAndAsh
import Game.GoreAndAsh.Logging
import Game.GoreAndAsh.Network
import Game.GoreAndAsh.Sync
import Graphics.Urho3D.Multithread

-- | Which network implementation to use
type AppNetworkBackend = UrhoNetworkBackend

-- | Peer connection for application monad
type AppPeer = Peer AppNetworkBackend

-- | Monad that is used all shared API between different environments of application
type FoundationMonad = SyncT Spider AppNetworkBackend (NetworkT Spider AppNetworkBackend (LoggingT Spider GMSpider))

-- | Application monad that is used before all systems are ready
type InitAppMonad = ReaderT Env FoundationMonad

-- | Application monad that is used for implementation of game API
type AppMonad = ReaderT (REnv Spider) FoundationMonad

-- | Fires when we ready to init reactive environment
initReadyEvent :: InitAppMonad (Event Spider ())
initReadyEvent = do
  initRef <- asks envInitialized
  (e, fire) <- newTriggerEvent
  _ <- liftIO . forkIO $ do
    takeMVar initRef
    fire ()
  pure e
