module Aerospace.Client.Camera(
    module Aerospace.Client.Camera.Basic
  , module Aerospace.Client.Camera.Class
  , module Aerospace.Client.Camera.Fly
  ) where

import Aerospace.Client.Camera.Basic
import Aerospace.Client.Camera.Class
import Aerospace.Client.Camera.Fly

-- -- | Raycast from given pos with given camera and return first object hit
-- cameraRaycastSingle :: (MonadIO m, MonadMask m, MonadReader Core m)
--   => CameraId -- ^ Camera to cast from
--   -> Float -- ^ Maximum hit distance
--   -> IntVector2 -- ^ Position in screen
--   -> m (Maybe RayQueryResult)
-- cameraRaycastSingle cid maxDistance (IntVector2 px py) = fmap join . withCamera cid $ \CameraData{..} -> do
--   graphics <- asks coreGraphics
--   octree <- asks coreOctree
--   width <- graphicsGetWidth graphics
--   height <- graphicsGetHeight graphics
--   cameraRay <- cameraGetScreenRay cameraPtr (fromIntegral px / fromIntegral width) (fromIntegral py / fromIntegral height)
--   -- Pick only geometry objects, not eg. zones or lights, only get the first (closest) hit
--   withObject (cameraRay, RayTriangle, maxDistance, drawableGeometry, defaultViewMask) $ \(query :: Ptr RayOctreeQuery) -> do
--     octreeRaycastSingle octree query
--     results <- rayOctreeQueryGetResult query
--     pure $ if V.null results then Nothing
--       else Just $ V.head results
--
-- -- | Raycast from cursor pos with given camera and return first object hit
-- cursorRaycastSingle :: (MonadIO m, MonadMask m, MonadReader Core m)
--   => CameraId -- ^ Camera to cast from
--   -> Float -- ^ Maximum hit distance
--   -> m (Maybe RayQueryResult)
-- cursorRaycastSingle cid maxDistance = fmap join . withCamera cid $ \CameraData{..} -> do
--   ui <- asks coreUI
--   cursor <- asks coreCursor
--   graphics <- asks coreGraphics
--   octree <- asks coreOctree
--   isVisible <- uiElementIsVisible cursor
--   pos@(IntVector2 px py) <- uiGetCursorPosition ui
--   mres <- uiGetElementAt ui pos True
--   if not isVisible || isJust mres then pure Nothing
--     else do
--       width <- graphicsGetWidth graphics
--       height <- graphicsGetHeight graphics
--       cameraRay <- cameraGetScreenRay cameraPtr (fromIntegral px / fromIntegral width) (fromIntegral py / fromIntegral height)
--       -- Pick only geometry objects, not eg. zones or lights, only get the first (closest) hit
--       withObject (cameraRay, RayTriangle, maxDistance, drawableGeometry, defaultViewMask) $ \(query :: Ptr RayOctreeQuery) -> do
--         octreeRaycastSingle octree query
--         results <- rayOctreeQueryGetResult query
--         pure $ if V.null results then Nothing
--           else Just $ V.head results
