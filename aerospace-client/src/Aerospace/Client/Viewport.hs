module Aerospace.Client.Viewport(
    ViewportConfig(..)
  , ViewportComponent(..)
  , initViewport
  ) where

import Aerospace.Client.Env.Reactive
import Aerospace.Util
import Foreign hiding (void)
import Game.GoreAndAsh
import Graphics.Urho3D hiding (Event)
import Reflex

data ViewportConfig t = ViewportConfig {
-- | Which camera attach viewport to
  viewportCfgCamera :: Dynamic t (Ptr Camera)
}

data ViewportComponent t = ViewportComponent {
-- | Pointer to created viewport
  viewportPtr :: SharedPtr Viewport
}

-- | Set up a viewport to the Renderer subsystem so that the 3D scene can be seen. We need to define the scene and the camera
-- at minimum. Additionally we could configure the viewport screen size and the rendering path (eg. forward / deferred) to
-- use, but now we just use full screen and default render path configured in the engine command line options
initViewport :: ReactiveMonad t m => ViewportConfig t -> m (Event t (ViewportComponent t))
initViewport ViewportConfig{..} = do
  app <- getApplication
  cntx <- getContext app
  scene <- getScene
  renderer <- getRenderer
  buildE <- getPostBuild
  cameraPtr <- sample . current $ viewportCfgCamera
  viewportE <- performMainThread $ ffor buildE $ const $ do
    (viewport :: SharedPtr Viewport) <- newSharedObject (cntx, pointer scene, cameraPtr)
    rendererSetViewport renderer 0 viewport
    pure viewport
  _ <- networkHold (pure ()) $ ffor viewportE $ \ptr ->
    performMainThread_ $ ffor (updated viewportCfgCamera) $ viewportSetCamera ptr
  pure $ ffor viewportE $ \ptr -> ViewportComponent {
      viewportPtr = ptr
    }
