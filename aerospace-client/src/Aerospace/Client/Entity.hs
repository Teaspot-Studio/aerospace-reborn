module Aerospace.Client.Entity(
    entityCollectionComponent
  , entityComponent
  , waitEntityImpl
  , newEntityCollectionEnv
  , module Aerospace.Client.Entity.Class
  ) where

import Aerospace.Client.Entity.Class
import Aerospace.Client.Env.Reactive

import Data.Time.Clock.POSIX
import Debug.Trace
import qualified Data.Map.Strict as M

-- | Handle collection of entities that are sent from server
entityCollectionComponent :: forall t m . ReactiveMonadCore t m => REnv t -> m (EntityCollection t)
entityCollectionComponent env = do
  buildE <- getPostBuild
  let tickDur = 0.01 :: Float
  tickE <- fmap (tickDur <$) $ tickEvery $ realToFrac tickDur
  rec
    initTickE <- tickEveryUntil 0.2 initEntsMsgsE
    msgE <- receiveFromServer entityCollectionId
    let addMsgE = fforMaybe msgE $ \case
          NewEntityMessage i -> Just [i]
          _ -> Nothing
        initEntsMsgsE = fforMaybe msgE $ \case
          ResponseEntitiesMessage is -> Just is
          _ -> Nothing
        delMsgE = fforMaybe msgE $ \case
          RemoveEntityMessage i -> Just i
          _ -> Nothing
        reqMsgE = RequestEntitiesMessage <$ leftmost [initTickE, buildE]
  --logDebugE $ showl <$> readyMsgE
  --logDebugE $ showl <$> addMsgE
  _ <- sendToServer entityCollectionId ReliableMessage reqMsgE
  let
    addE :: Event t (Map EntityId (Maybe ()))
    addE = ffor (addMsgE <> initEntsMsgsE) $ \is -> M.fromList $ (, Just ()) <$> is

    delE :: Event t (Map EntityId (Maybe ()))
    delE = ffor delMsgE $ \i -> M.singleton i Nothing

    updE :: Event t (Map EntityId (Maybe ()))
    updE = addE <> delE

    maker :: EntityId -> () -> Event t () -> m (Event t (ClientEntity t))
    maker i _ _ = flip runReaderT env $ entityComponent ClientEntityCfg {
        centityCfgId = i
      , centityCfgParent = Nothing
      , centityCfgRemove = void $ fmapMaybe (M.lookup i) delE
      , centityCfgTick = tickE
      }
  tempMapD <- listWithKeyShallowDiff mempty updE maker
  res <- holdDynMap tempMapD
  -- logDebugE $ showl . void <$> updated res
  pure res

-- | Start syncing of entity
entityComponent :: forall t m . ReactiveMonad t m => ClientEntityCfg t -> m (Event t (ClientEntity t))
entityComponent ClientEntityCfg{..} = do
  scene <- getScene
  cache <- getResourceCache
  buildE <- getPostBuild
  nodeE <- performMainThread $ ffor buildE $ const $ do
    let nodeName = "Entity " ++ show (unEntityId centityCfgId)
        parentPtr = fromMaybe (parentPointer scene) centityCfgParent
    entNode <- nodeCreateChild parentPtr nodeName CMLocal 0
    -- DEBUG
    (boxObject :: Ptr StaticModel) <- guardNothing "Box StaticModel" =<< nodeCreateComponent entNode Nothing Nothing
    (boxModel :: Ptr Model) <- guardNothing "Box.mdl" =<< cacheGetResource cache "Models/Box.mdl" True
    staticModelSetModel boxObject boxModel
    (boxMaterial :: Ptr Material) <- guardNothing "Stone.xml" =<< cacheGetResource cache "Materials/Stone.xml" True
    staticModelSetMaterial boxObject boxMaterial
    -- END DEBUG
    pure entNode
  -- Handle removal of node
  mnodeD <- holdDyn Nothing $ Just <$> nodeE
  let remWithNodeE = fmapMaybe id $ tag (current mnodeD) centityCfgRemove
  performMainThread_ $ nodeRemove <$> remWithNodeE
  -- Sync from server
  entityD <- syncEntity
  -- Wait for creation of node
  networkHoldE $ ffor nodeE $ \enode -> do
    initE <- delay 0.01 =<< getPostBuild
    -- Prediction
    predPosD <- predictPos (entityVelocity entityD) $ entityPosition entityD
    -- logDebugE $ showl <$> updated predPosD
    -- Sync node position
    performMainThread_ $ ffor (updated predPosD) $ nodeSetPosition enode
    performMainThread_ $ ffor (updated $ entityRotation entityD) $  \(Vector3 pa ya ra) -> nodeSetRotation enode $ quaternionFromEuler pa ya ra
    -- Return result
    pure $ ffor initE $ const ClientEntity {
        centityId = centityCfgId
      , centityPosition = predPosD
      , centityVelocity = entityVelocity entityD
      , centityRotation = entityRotation entityD
      , centityNode = enode
      }
  where
    defEntityD = Entity {
        entityPosition = pure 0
      , entityVelocity = pure 0
      , entityRotation = pure 0
      }

    syncEntity :: m (EntityD t)
    syncEntity = fmap joinEntity $ syncWithName (entitySyncName centityCfgId) defEntityD $ do
      let sync i a = syncFromServer i a
      entityPosition <- sync entityPositionId 0
      entityVelocity <- sync entityVelocityId 0
      entityRotation <- sync entityRotationId 0
      pure Entity{..}

    -- Complex prediction of entity position, that assumes stable movement + interpolation
    -- of steep server rejects.
    predictPos :: Dynamic t Vector3 -> Dynamic t Vector3 -> m (Dynamic t Vector3)
    predictPos _ posD = linearInterpolate 50 0.1 posD

    -- predictPos velDyn serverPos = do
    --   let
    --     stepper :: Vector3 -> m (Dynamic t Vector3)
    --     stepper pd = do
    --       let f dt acc = do
    --             vel <- sample . current $ velDyn
    --             pure $ acc + (Vector3 dt dt dt) * vel
    --       foldDynM f pd centityCfgTick
    --   fmap join $ networkHold (pure serverPos) $ fmap stepper (updated serverPos)

-- | Default implementation for 'waitEntity' of 'EntityCollectionMonad'
waitEntityImpl :: ReactiveMonadCore t m => EntityCollectionEnv t -> Event t EntityId -> m (Event t (ClientEntity t))
waitEntityImpl env e = do
  let es = envEntityCollection env
      initFoundE = flip push e $ \i -> do
        emap <- sample . current $ es
        traceM $ show $ void emap
        pure $ M.lookup i emap
  let watchOurEntity i = do
        buildE <- delay 0.01 =<< getPostBuild
        let initE = flip push buildE $ const $ do
              m <- sample . current $ es
              pure $ M.lookup i m
            updateE = fmapMaybe (M.lookup i) $ updated es
        pure $ leftmost [updateE, initE]
  rec resE <- networkHoldE $ leftmost [
          watchOurEntity <$> e
        , pure never <$ resE
        , pure never <$ initFoundE
        ]
  headE $ leftmost [initFoundE, resE]

-- | Creation of new environment for 'EntityCollectionMonad'
newEntityCollectionEnv :: ReactiveMonadCore t m => REnv t -> m (EntityCollectionEnv t)
newEntityCollectionEnv env = do
  coll <- entityCollectionComponent env
  pure EntityCollectionEnv {
      envEntityCollection = coll
    }
