module Aerospace.Client.Config(
    Config(..)
  , loadConfig
  , storeConfig
  ) where

import Aerospace.Aeson
import Control.Monad.IO.Class
import Data.Data
import Data.Yaml.Config
import Data.Yaml.Pretty (encodePretty, defConfig, setConfDropNull, setConfCompare)

import qualified Data.ByteString as BS

-- | Config file that stores saved options of application
data Config = Config {
  configFullScreen :: !Bool
} deriving (Show)

deriveJSON defaultOptions ''Config

-- | Load config from file and absolutize paths in int
loadConfig :: MonadIO m => FilePath -> m Config
loadConfig path = liftIO $ loadYamlSettings [path] [] useEnv

-- | Store config to the given file
storeConfig :: MonadIO m => FilePath -> Config -> m ()
storeConfig path cfg = liftIO $ BS.writeFile path $ encodePretty ecfg cfg
  where
    ecfg = setConfDropNull True $ setConfCompare compare $ defConfig
