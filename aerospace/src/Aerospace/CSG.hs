{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
-- | Types and generic algorithms for constructive solid geometry operations.
module Aerospace.CSG(
    ScaledPath
  , Solid(..)
  , IntersectSolid(..)
  , renderSolid
  , solidToBsp
  -- * Helpers
  , cube
  , cylinder
  , torus
  ) where

import Aerospace.BSP
import Aerospace.CSG.Extrude
import Aerospace.CSG.Polyhedron
import Aerospace.CSG.Sphere
import Aerospace.Polygon
import Aerospace.Quaternion
import Aerospace.Triangle
import Aerospace.Vector3
import Control.DeepSeq
import Data.Map.Strict (Map)
import Data.Semigroup
import Data.Vector.Unboxed (Vector, Unbox)
import GHC.Generics

import qualified Data.Vector.Unboxed as V

-- | Allowed primitives for CSG model and their composition.
data Solid a =
  -- | Ideal tetrahedron with center at origin and length of side equals 1
    Tetrahedron !a
  -- | Ideal octahedron with center at origin and length of side equals 1
  | Octahedron !a
  -- | Ideal dodecahedron with center at origin and length of side equals 1
  | Dodecahedron !a
  -- | Ideal icosahedron with center at origin and length of side equals 1
  | Icosahedron !a
  -- | Sphere with center at origin and radius equals 1.
  | Sphere !a
  -- | Defines prism by basement polygon, path of extrusion with scales for each step.
  -- This can produce cube, cylinder, prism, cone, torus and other figures.
  -- Boolean controls enclosing parts of resulted solid, if 'True' the begining and
  -- ending polygons are added to the figure, either the figure considered as enclosed
  -- (start is connected with ending).
  | Extrude !Polygon !ScaledPath !Bool !a
  -- | Rotated primitive with a quaternion
  | Rotated !Quaternion !(Solid a)
  -- | Moved primitive by a vector
  | Translated !Vector3 !(Solid a)
  -- | Scaled primitive by a vector
  | Scaled !Vector3 !(Solid a)
  -- | Union of two figures
  | Union !(Solid a) !(Solid a)
  -- | Intersection of two figures
  | Intersect !(Solid a) !(Solid a)
  -- | Subtract space from first figure using the second
  | Subtract !(Solid a) !(Solid a)
  -- | Flipping solid space with outter space
  | Complement !(Solid a)
  deriving (Show, Generic, Functor, Foldable, Traversable)

instance NFData a => NFData (Solid a)

instance Semigroup (Solid a) where
  (<>) = Union
  {-# INLINE (<>) #-}

-- | Wrapper around 'Solid' that make intesections on semigroup operations
newtype IntersectSolid a = IntersectSolid { unIntersectSolid :: Solid a }
  deriving (Show, Generic, Functor, Foldable, Traversable)

instance Semigroup (IntersectSolid a) where
  (IntersectSolid a) <> (IntersectSolid b) = IntersectSolid (Intersect a b)
  {-# INLINE (<>) #-}

-- | Convert given solid object into several geometries. Here `a` can be
-- material indecies to render parts of model with different colors/shaders.
renderSolid :: (Unbox a, Eq a, Ord a) => Solid a -> Map a (Vector (VertexTriangle a))
renderSolid = bspTrianglesMap . solidToBsp
{-# INLINE renderSolid #-}

-- | Convert given solid object to BSP node. Actually here triangles of
-- primitives are processed.
solidToBsp :: Unbox a => Solid a -> BSPNode a
solidToBsp s = case s of
  Tetrahedron v         -> bspPrimitive $ tetrahedronPrimitive v
  Octahedron v          -> bspPrimitive $ octahedronPrimitive v
  Dodecahedron v        -> bspPrimitive $ dodecahedronPrimitive v
  Icosahedron v         -> bspPrimitive $ icosahedronPrimitive v
  Sphere v              -> bspPrimitive $ spherePrimitive 1 v
  Extrude pol pth clc v -> bspPrimitive $ extrudedPrimitive pol pth clc v
  Rotated q sv          -> bspRotate q (solidToBsp sv)
  Translated t sv       -> bspTranslate t (solidToBsp sv)
  Scaled scl sv         -> bspScale scl (solidToBsp sv)
  Union as bs           -> bspUnion (solidToBsp as) (solidToBsp bs)
  Intersect as bs       -> bspIntersect (solidToBsp as) (solidToBsp bs)
  Subtract as bs        -> bspSubtract (solidToBsp as) (solidToBsp bs)
  Complement sv         -> bspInvert $ solidToBsp sv
{-# INLINE solidToBsp #-}

-- | Make cube with sides equals 1
cube :: a -> Solid a
cube a = Extrude squarePolygon (V.fromList [(Vector3 0 1 0, zeroRotation, 1)]) True a
  where
    squarePolygon = Polygon $ V.fromList [
        Vector3 (-0.5) (-0.5) 0.5
      , Vector3 0.5 (-0.5) 0.5
      , Vector3 0.5 (-0.5) (-0.5)
      , Vector3 (-0.5) (-0.5) (-0.5)
      ]

-- | Make cylinder with radius 0.5 and height 1
cylinder :: Int -- ^ Number of points in circle
  -> a -> Solid a
cylinder cn a = Extrude spoly (V.fromList [(Vector3 0 1 0, zeroRotation, 1)]) True a
  where
    spoly = translatePolygon (Vector3 0 (-0.5) 0) $ scalePolygon 0.5 $ circlePolygon cn

-- | Make a torus
torus :: Int -- ^ Number of torus steps, the more steps the more smouth the torus will be
  -> Int -- ^ Number of circle steps, the more steps, the more smouth circle would be in base of torus
  -> Float -- ^ Internal radius
  -> Float -- ^ External radius
  -> a -> Solid a
torus tn cn ir er a = Extrude spoly (V.fromList path) False a
  where
    rotToXY = quaternionFromAxis (Vector3 (-1) 0 0) (pi / 2)
    cr = (er - ir) / 2
    spoly = translatePolygon (Vector3 (cr + ir) 0 0) $ rotatePolygon rotToXY $ scalePolygon (Vector3 cr cr cr) $ circlePolygon cn
    centroid = polygonCentroid spoly
    path = mkStep <$> [0 .. tn - 1]
    stepAxis = Vector3 0 1 0
    dang = 2 * pi / fromIntegral tn
    mkStep i = let
      angOld = fromIntegral i * dang
      angNew = fromIntegral (i + 1) * dang
      angDelta = angNew - angOld
      rotDelta = quaternionFromAxis stepAxis angDelta
      posOld = quaternionRotate (quaternionFromAxis stepAxis angOld) centroid
      posNew = quaternionRotate (quaternionFromAxis stepAxis angNew) centroid
      posDelta = posNew - posOld
      in (posDelta, rotDelta, 1)
