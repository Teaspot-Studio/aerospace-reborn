module Aerospace.Vector2(
    module Graphics.Urho3D.Math.Vector2
  , module Aerospace.Vector
  ) where

import Aerospace.Vector
import Data.Vector.Unboxed.Deriving
import Graphics.Urho3D.Math.Vector2

derivingUnbox "Vector2"
  [t| Vector2 -> (Float, Float) |]
  [| \(Vector2 v1 v2) -> (v1, v2) |]
  [| \(v1, v2) -> Vector2 v1 v2 |]

type instance VectorElement Vector2 = Float

instance ScaleVector Vector2 where
  scalev (Vector2 vx vy) v = Vector2 (vx*v) (vy*v)
  {-# INLINE scalev #-}

instance LengthVector Vector2 where
  lengthv (Vector2 vx vy) = vx*vx + vy*vy
  {-# INLINE lengthv #-}

instance DotVector Vector2 where
  dotv (Vector2 vx1 vy1) (Vector2 vx2 vy2) = vx1*vx2 + vy1*vy2
  {-# INLINE dotv #-}

instance NormalizeVector Vector2
instance LerpVector Vector2
