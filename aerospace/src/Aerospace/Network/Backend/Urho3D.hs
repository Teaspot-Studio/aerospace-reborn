module Aerospace.Network.Backend.Urho3D(
    UrhoNetworkBackend
  , UrhoNetworkOpts(..)
  , UrhoNetworkConnectOpts(..)
  , UrhoNetworkCreateError(..)
  , UrhoNetworkConnectError(..)
  , UrhoNetworkError(..)
  , UrhoNetworkSendError(..)
  ) where

import Control.Monad.Reader
import Data.ByteString (ByteString)
import Data.Foldable (traverse_)
import Data.Maybe
import Foreign hiding (void)
import Game.GoreAndAsh.Network.Backend
import GHC.Generics
import Graphics.Urho3D
import Graphics.Urho3D.Multithread
import Graphics.Urho3D.Network

import qualified Data.ByteString.Char8 as BS
import qualified Graphics.Urho3D.Network as U

-- | Network backend based on Urho3D network system
data UrhoNetworkBackend

-- | Specific options of Urho backend
data UrhoNetworkOpts = UrhoNetworkOpts {
  -- | Urho application reference
  urhoNetworkApp    :: SharedPtr Application
  -- | Start server on port
, urhoNetworkServer :: Maybe Int
  -- | Debug. Adds fixed delay for each packet.
, urhoNetworkSimulatedLatency :: Maybe Int
  -- | Debug. Probality of packet loss from 0.0 to 1.0
, urhoNetworkSimulatedLoss :: Maybe Float
} deriving (Generic)

-- | Connection to server options
data UrhoNetworkConnectOpts = UrhoNetworkConnectOpts {
  -- | UDP port of remote server
  urhoNetworkConnectPort :: Int
} deriving (Generic)

-- | Errors of Urho network
data UrhoNetworkCreateError =
  -- | When get subystem method returns null
    FailedToGetNetworkSubsystem
  -- | When filed to init server listening
  | FailedToStartListen
  deriving (Generic, Show)

-- | Errors of Urho network, connection failed
data UrhoNetworkConnectError = UrhoNetworkConnectFail
  deriving (Generic, Show)

-- | Errors of Urho network
data UrhoNetworkError = UrhoNetworkErrorStub
  deriving (Generic, Show)

-- | Errors of Urho network
data UrhoNetworkSendError = UrhoNetworkSendErrorStub
  deriving (Generic, Show)

-- | Peer is either server connection or remote client
data UrhoPeer = UrhoServerPeer | UrhoClientPeer (Ptr Connection)
  deriving (Eq, Ord, Show)

-- | ID of urho message that corresponds to ingame messages
urhoOurMessageId :: ChannelId -> Int
urhoOurMessageId c = 42 + (fromIntegral $ unChannelId c)

-- | Convert from urho message id to our channel id
toChannelId :: Int -> ChannelId
toChannelId i = ChannelId $ fromIntegral i - 42

-- | Does message requires reliability of delivery
reliableFlag :: MessageType -> Bool
reliableFlag mt = case mt of
  ReliableMessage -> True
  UnreliableMessage -> False
  UnsequencedMessage -> False
  UnreliableFragmentedMessage -> False
  UnsequencedFragmentedMessage -> False

-- | Does message requires ordering on receive
keepOrderFlag :: MessageType -> Bool
keepOrderFlag mt = case mt of
  ReliableMessage -> True
  UnreliableMessage -> True
  UnsequencedMessage -> False
  UnreliableFragmentedMessage -> True
  UnsequencedFragmentedMessage -> False

instance HasNetworkBackend UrhoNetworkBackend where
  type Peer UrhoNetworkBackend = UrhoPeer
  type BackendOptions UrhoNetworkBackend = UrhoNetworkOpts
  type ConnectOptions UrhoNetworkBackend = UrhoNetworkConnectOpts

  type BackendCreateError UrhoNetworkBackend = UrhoNetworkCreateError
  type BackendConnectError UrhoNetworkBackend = UrhoNetworkConnectError
  type BackendEventError UrhoNetworkBackend = UrhoNetworkError
  type BackendSendError UrhoNetworkBackend = UrhoNetworkSendError

  createNetworkBackend ctx = liftIO $ do
    menv <- newUrhoNetworkEnv ctx
    case menv of
      Left er -> pure $ Left er
      Right env -> do
        let run f = runReaderT f env
        pure $ Right NetworkBackend {
            networkConnect = \addr copts -> run $ urhoConnect addr copts
          , networkDisconnect = \peer -> run $ urhoDisconnect peer
          , networkSendMessage = \peer ch mt bs -> run $ urhoSendMessage peer ch mt bs
          , networkTerminate = run urhoTerminate
          }

-- | Environment accessed in backend implementation
data UrhoNetworkEnv = UrhoNetworkEnv {
  envNetwork    :: !(Ptr Network)
, envBackendCtx :: !(NetworkBackendContext UrhoNetworkBackend)
, envIsServer   :: !Bool
}

-- | Make new env of monad
newUrhoNetworkEnv :: NetworkBackendContext UrhoNetworkBackend -> IO (Either UrhoNetworkCreateError UrhoNetworkEnv)
newUrhoNetworkEnv ctx = do
  let UrhoNetworkOpts{..} = networkBackendOptions ctx
  mnetwork <- getSubsystem urhoNetworkApp
  case mnetwork of
    Nothing -> pure $ Left FailedToGetNetworkSubsystem
    Just nptr -> do
      let env = UrhoNetworkEnv {
          envNetwork = nptr
        , envBackendCtx = ctx
        , envIsServer = isJust urhoNetworkServer
        }
      let run f = runReaderT f env
      subscribeToEvent urhoNetworkApp $ run . urhoServerConnected
      subscribeToEvent urhoNetworkApp $ run . urhoServerDisconnected
      subscribeToEvent urhoNetworkApp $ run . urhoConnectFailed
      subscribeToEvent urhoNetworkApp $ run . urhoClientConnected
      subscribeToEvent urhoNetworkApp $ run . urhoClientDisconnected
      subscribeToEvent urhoNetworkApp $ run . urhoNetworkMessage
      _ <- whenJust urhoNetworkSimulatedLatency $ networkSetSimulatedLatency nptr
      _ <- whenJust urhoNetworkSimulatedLoss $ networkSetSimulatedPacketLoss nptr
      case urhoNetworkServer of
        Just p -> do
          res <- networkStartServer nptr (fromIntegral p)
          pure $ if res then Right env
            else Left FailedToStartListen
        _ -> pure $ Right env

-- | Shortcut for internal implementation monad
type UrhoNetworkM a = ReaderT UrhoNetworkEnv IO a

urhoConnect :: RemoteAddress -> UrhoNetworkConnectOpts -> UrhoNetworkM ()
urhoConnect raddr UrhoNetworkConnectOpts{..} = do
  netp <- asks envNetwork
  runInMainThread . void $ U.networkConnect netp (BS.unpack raddr) (fromIntegral urhoNetworkConnectPort) nullPtr

urhoDisconnect :: UrhoPeer -> UrhoNetworkM ()
urhoDisconnect upeer = case upeer of
  UrhoServerPeer -> do
    netp <- asks envNetwork
    runInMainThread $ U.networkDisconnect netp 0
  UrhoClientPeer peer -> do
    ctx <- asks envBackendCtx
    runInMainThread $ do
      U.connectionDisconnect peer 0
      networkTriggerRemoteDisonnected ctx upeer

urhoSendMessage :: UrhoPeer -> ChannelId -> MessageType -> ByteString -> UrhoNetworkM ()
urhoSendMessage upeer cid mt bs = do
  let send sconn = runInMainThread $ connectionSendMessage sconn (urhoOurMessageId cid) (reliableFlag mt) (keepOrderFlag mt) bs 0
  case upeer of
    UrhoServerPeer -> do
      netp <- asks envNetwork
      msconn <- networkGetServerConnection netp
      traverse_ send msconn
    UrhoClientPeer peer -> send peer

urhoTerminate :: UrhoNetworkM ()
urhoTerminate = do
  isServer <- asks envIsServer
  netp <- asks envNetwork
  ctx <- asks envBackendCtx
  if isServer
    then runInMainThread $ do
      cs <- networkGetClientConnections netp
      let disconn sptr = do
            let ptr = pointer sptr
            U.connectionDisconnect ptr 0
            networkTriggerRemoteDisonnected ctx $ UrhoClientPeer ptr
      traverse_ disconn cs
      U.networkStopServer netp
    else runInMainThread $ do
      msconn <- networkGetServerConnection netp
      let disconn ptr = do
            U.connectionDisconnect ptr 0
            liftIO $ networkTriggerLocalDisonnected ctx
      traverse_ disconn msconn

urhoServerConnected :: EventServerConnected -> UrhoNetworkM ()
urhoServerConnected _ = do
  ctx <- asks envBackendCtx
  liftIO $ networkTriggerLocalConnected ctx UrhoServerPeer

urhoServerDisconnected :: EventServerDisconnected -> UrhoNetworkM ()
urhoServerDisconnected _ = do
  ctx <- asks envBackendCtx
  liftIO $ networkTriggerLocalDisonnected ctx

urhoConnectFailed :: EventConnectFailed -> UrhoNetworkM ()
urhoConnectFailed _ = do
  ctx <- asks envBackendCtx
  liftIO $ networkTriggerConnectionError ctx (UrhoNetworkConnectFail, "")

urhoClientConnected :: EventClientConnected -> UrhoNetworkM ()
urhoClientConnected EventClientConnected{..} = do
  ctx <- asks envBackendCtx
  liftIO $ networkTriggerRemoteConnected ctx $ UrhoClientPeer eventClientConnectedConnection

urhoClientDisconnected :: EventClientDisconnected -> UrhoNetworkM ()
urhoClientDisconnected EventClientDisconnected{..} = do
  ctx <- asks envBackendCtx
  liftIO $ networkTriggerRemoteDisonnected ctx $ UrhoClientPeer eventClientDisconnectedConnection

urhoNetworkMessage :: EventNetworkMessage -> UrhoNetworkM ()
urhoNetworkMessage EventNetworkMessage{..} = do
  ctx <- asks envBackendCtx
  isServer <- asks envIsServer
  let peer = if isServer
        then UrhoClientPeer eventNetworkMessageConnection
        else UrhoServerPeer
      chan = toChannelId eventNetworkMessageId
  liftIO $ networkTriggerIncomingMessage ctx peer chan ReliableMessage eventNetworkMessageData
