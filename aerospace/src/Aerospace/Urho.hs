-- | Here goes generic primitives to work with Urho3D engine.
--
-- We use intentionaly use ExceptT instead of exceptions to force processing of
-- errors, either forgoten exceptions will silently kill reactive network of the game.
module Aerospace.Urho(
    HasContext(..)
  , HasResourceCache(..)
  , HasScene(..)
  , UrhoError(..)
  , renderUrhoError
  , getResource
  , wrapNothing
  ) where

import Aerospace.Util
import Control.Monad.Except
import Control.Monad.Reader
import Data.Monoid
import Data.Text (Text, unpack)
import Foreign
import GHC.Generics
import Graphics.Urho3D hiding (Text, HasContext)

-- | Monadic context that has access to Urho context
class Monad m => HasContext m where
  getUrhoContext :: m (Ptr Context)

instance HasContext m => HasContext (ExceptT e m) where
  getUrhoContext = lift getUrhoContext
  {-# INLINE getUrhoContext #-}

instance {-# OVERLAPPABLE #-} HasContext m => HasContext (ReaderT e m) where
  getUrhoContext = lift getUrhoContext
  {-# INLINE getUrhoContext #-}

-- | Monadic context that has access to resouce cache
class Monad m => HasResourceCache m where
  getResourceCache :: m (Ptr ResourceCache)

instance HasResourceCache m => HasResourceCache (ExceptT e m) where
  getResourceCache = lift getResourceCache
  {-# INLINE getResourceCache #-}

instance {-# OVERLAPPABLE #-} HasResourceCache m => HasResourceCache (ReaderT e m) where
  getResourceCache = lift getResourceCache
  {-# INLINE getResourceCache #-}

-- | Monadic context that has access to current scene
class Monad m => HasScene m where
  getScene :: m (SharedPtr Scene)

instance Monad m => HasScene (ReaderT (SharedPtr Scene) m) where
  getScene = ask
  {-# INLINE getScene #-}

instance HasScene m => HasScene (ExceptT e m) where
  getScene = lift getScene
  {-# INLINE getScene #-}

instance {-# OVERLAPPABLE #-} HasScene m => HasScene (ReaderT e m) where
  getScene = lift getScene
  {-# INLINE getScene #-}

-- | Engine errors
data UrhoError =
    MissingResourceError Text
  | MissingMaterialLocalId Int
  deriving (Eq, Show, Generic)

-- | Pretty print error for logs
renderUrhoError :: UrhoError -> Text
renderUrhoError e = case e of
  MissingResourceError msg -> "Missing resource " <> msg
  MissingMaterialLocalId i -> "Missing material with local id " <> showt i

-- | Wrapper to get resource from cache with catching of errors
getResource :: (MonadIO m, HasResourceCache m, MonadError UrhoError m, ResourceType a) => Text -> m (Ptr a)
getResource resname = do
  cache <- getResourceCache
  wrapNothing (MissingResourceError resname) $ cacheGetResource cache (unpack resname) True

-- | Helper to catch missing values as errors
wrapNothing :: MonadError UrhoError m => UrhoError -> m (Maybe a) -> m a
wrapNothing e ma = do
  a <- ma
  maybe (throwError e) pure a
