-- | Operations with geometrical planes
module Aerospace.Plane(
    Plane(..)
  , planeEpsilon
  , planeFromPoints
  , planeFromTriangle
  , planeFromNormal
  , planeFlip
  , planeRotate
  , planeTranslate
  , planeScale
  , PlanePosition(..)
  , checkPlainPoint
  , checkPlainTriangle
  , isTriangleSameDirection
  , SplitResult(..)
  , splitTriangle
  ) where

import Aerospace.Quaternion
import Aerospace.Triangle
import Aerospace.Vector3
import Aerospace.Vertex
import Control.DeepSeq
import Data.Semigroup
import Data.Vector.Unboxed (Vector, Unbox)
import GHC.Generics

import qualified Data.Vector.Unboxed as V

-- | Plane is defined as normal vector and distance from origin
data Plane = Plane {
-- | Normal vector of plane
  planeNormal   :: {-# UNPACK #-} !Vector3
-- | Distance to plane from origin (actually in canonic equation is negated)
, planeDistance :: {-# UNPACK #-} !Float
} deriving (Show, Generic)

instance NFData Plane

-- | Tolerance used for splitting triangles to decide if a point on the plane
planeEpsilon :: Float
planeEpsilon = 1e-5
{-# INLINE planeEpsilon #-}

-- | Define plane by three points
planeFromPoints :: Vector3 -> Vector3 -> Vector3 -> Plane
planeFromPoints a b c = Plane {
    planeNormal = n
  , planeDistance = n `dotv` a
  }
  where
    n = normalizev $ (b - a) `crossv` (c - a)
{-# INLINE planeFromPoints #-}

-- | Define plane by triangle points
planeFromTriangle :: VertexTriangle a -> Plane
planeFromTriangle tr = Plane {
    planeNormal = n
  , planeDistance = n `dotv` vertexPosition (vtriangleFirst tr)
  }
  where
    n = triangleNormal tr
{-# INLINE planeFromTriangle #-}

-- | Plane from point and normal
planeFromNormal :: Vector3 -> Vector3 -> Plane
planeFromNormal r0 n0 = Plane {
    planeNormal = n
  , planeDistance = n `dotv` r0
  }
  where
    n = normalizev n0
{-# INLINE planeFromNormal #-}

-- | Flip directional data for plane
planeFlip :: Plane -> Plane
planeFlip p = p {
    planeNormal = negate $ planeNormal p
  , planeDistance = negate $ planeDistance p
  }
{-# INLINE planeFlip #-}

-- | Rotate plane with given quaternion
planeRotate :: Quaternion -> Plane -> Plane
planeRotate q p = p {
    planeNormal = quaternionRotate q $ planeNormal p
  }
{-# INLINE planeRotate #-}

-- | Move plane origin across vector
planeTranslate :: Vector3 -> Plane -> Plane
planeTranslate t p = planeFromNormal r1 (planeNormal p)
  where
    r0 = planeNormal p `scalev` planeDistance p
    r1 = r0 + t
{-# INLINE planeTranslate #-}

-- | Scale plane
planeScale :: Vector3 -> Plane -> Plane
planeScale t p = p {
    planeNormal = n
  , planeDistance = n `dotv` r0
  }
  where
    n = normalizev $ t * planeNormal p
    r0 = planeNormal p `scalev` planeDistance p
{-# INLINE planeScale #-}


-- | Position relative to a plane
data PlanePosition =
    Coplonar -- ^ In same plane
  | Front -- ^ In front of plane
  | Back -- ^ In back of plane
  | Spanning -- ^ Crossing the plane
  deriving (Show, Eq, Generic)

instance Semigroup PlanePosition where
  Coplonar <> v  = v
  v <> Coplonar  = v
  Spanning <> _  = Spanning
  _ <> Spanning  = Spanning
  Front <> Back  = Spanning
  Back  <> Front = Spanning
  Front <> Front = Front
  Back  <> Back  = Back
  {-# INLINE (<>) #-}

-- | Test if the point is in plane or in front or back relative to the plane
--
-- Note: point cannot span acros a plane.
checkPlainPoint :: Vector3 -> Plane -> PlanePosition
checkPlainPoint v p
  | t < - planeEpsilon = Back
  | t >   planeEpsilon = Front
  | otherwise = Coplonar
  where t = planeNormal p `dotv` v - planeDistance p
{-# INLINE checkPlainPoint #-}

-- | Test if the triangle is coplonar with the plane or in front or back of the plane.
checkPlainTriangle :: VertexTriangle a -> Plane -> PlanePosition
checkPlainTriangle tr p = t1 <> t2 <> t3
  where
    t1 = checkPlainPoint (vertexPosition $ vtriangleFirst tr) p
    t2 = checkPlainPoint (vertexPosition $ vtriangleSecond tr) p
    t3 = checkPlainPoint (vertexPosition $ vtriangleThird tr) p
{-# INLINE checkPlainTriangle #-}

-- | Test if the triangle facing the same direction that the plane
isTriangleSameDirection :: VertexTriangle a -> Plane -> Bool
isTriangleSameDirection tr p = (planeNormal p `dotv` triangleNormal tr) > 0

-- | Result of 'splitTriangle'
data SplitResult a =
  -- | Triangle is completely in plane and faces along normal of plane
    SplitCoplonarFront
  -- | Triangle is completely in plane and faces opposite normal of plane
  | SplitCoplonarBack
  -- | Triangle is completely in front of plane
  | SplitFront
  -- | Triangl is completely backward relative to the plane
  | SplitBack
  -- | Triangle is crossing the plane, first result is splitted parts in front of plane,
  -- the second is splitted parts in back of plane
  | SplitSpanning !(Vector (VertexTriangle a)) !(Vector (VertexTriangle a))
  deriving (Show, Generic)

-- | Split the triangle with given plane
splitTriangle :: forall a . Unbox a => Plane -> VertexTriangle a -> SplitResult a
splitTriangle p tr = case checkPlainTriangle tr p of
  Coplonar -> if isTriangleSameDirection tr p
    then SplitCoplonarFront
    else SplitCoplonarBack
  Front -> SplitFront
  Back -> SplitBack
  Spanning -> SplitSpanning (combine back) (combine front)
    where (back, front) = splitSegment (vtriangleFirst tr) (vtriangleSecond tr)
                `mergevs` splitSegment (vtriangleSecond tr) (vtriangleThird tr)
                `mergevs` splitSegment (vtriangleThird tr) (vtriangleFirst tr)
  where
    combine :: Vector Vertex -> Vector (VertexTriangle a)
    combine vs
      | V.length vs == 4 = V.fromList [tr1, tr2]
      | V.length vs == 3 = V.singleton tr1
      | otherwise = error "Impossible vertex count when splitting!"
      where
        v1 = vs V.! 0
        v2 = vs V.! 1
        v3 = vs V.! 2
        v4 = vs V.! 3
        tr1 = VertexTriangle v1 v2 v3 (vtriangleCustom tr)
        tr2 = VertexTriangle v1 v3 v4 (vtriangleCustom tr)

    mergevs :: (Vector Vertex, Vector Vertex) -> (Vector Vertex, Vector Vertex) -> (Vector Vertex, Vector Vertex)
    mergevs (!a1, !b1) (!a2, !b2) = (a1 V.++ a2, b1 V.++ b2)

    splitSegment :: Vertex -> Vertex -> (Vector Vertex, Vector Vertex)
    splitSegment a b = (back, front)
      where
        at = checkPlainPoint (vertexPosition a) p
        bt = checkPlainPoint (vertexPosition b) p
        backInit  = if at /= Back then V.singleton a else V.empty
        frontInit = if at /= Front then V.singleton a else V.empty
        back = backInit V.++ spanning
        front = frontInit V.++ spanning
        spanning = if at <> bt == Spanning
          then V.singleton (interpolateVertex t a b)
          else V.empty
        t = (planeDistance p - dotv (planeNormal p) (vertexPosition a))
          / (planeNormal p `dotv` (vertexPosition b - vertexPosition a))
