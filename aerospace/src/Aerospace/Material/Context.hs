module Aerospace.Material.Context(
    MonadMaterials(..)
  , HasMaterials(..)
  ) where

import Aerospace.Material.Types
import Control.Monad.Except
import Control.Monad.Reader
import Reflex

-- | Non reactive context that has access to material mapping
class Monad m => HasMaterials m where
  askMaterials :: m MaterialMap

instance HasMaterials m => HasMaterials (ExceptT e m) where
  askMaterials = lift askMaterials
  {-# INLINE askMaterials #-}

instance {-# OVERLAPPABLE #-} HasMaterials m => HasMaterials (ReaderT e m) where
  askMaterials = lift askMaterials
  {-# INLINE askMaterials #-}

-- | Reactive context where we have access to material registry
class Monad m => MonadMaterials t m | m -> t where
  -- | Get collection of materials with updates about changes
  getAllMaterials :: m (Dynamic t MaterialMap)
  -- | Registry new material
  registryMaterial :: Event t Material -> m (Event t MaterialLocalId)
  -- | Remove material from registry
  removeMaterial :: Event t MaterialLocalId -> m (Event t ())
