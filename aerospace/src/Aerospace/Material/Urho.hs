module Aerospace.Material.Urho(
    createEngineMaterial
  ) where

import Aerospace.Color
import Aerospace.Material.Types
import Aerospace.Urho
import Aerospace.Util
import Control.Monad.Except
import Data.IntMap (IntMap)
import Data.Monoid
import Data.Text (Text)
import Data.UUID (UUID)
import Foreign

import qualified Data.Text as T
import qualified Graphics.Urho3D as Urho
import qualified Graphics.Urho3D.Graphics.Technique as Urho

-- | Create underlying graphical engine material from gameplay material
createEngineMaterial :: (MonadIO m, MonadError UrhoError m, HasContext m, HasResourceCache m)
  => Material
  -> m (Ptr Urho.Material)
createEngineMaterial Material{..} = do
  ctx <- getUrhoContext
  pmat <- Urho.newObject ctx
  Urho.materialSetNumTechniques pmat 2
  when materialPackedNormal $ Urho.materialSetPixelShaderDefines pmat "PACKEDNORMAL"
  whenJust materialDiffuseTex $ \texname -> do
    diffTech :: Ptr Urho.Technique <- getResource "Techniques/Diff.xml"
    diffTex :: Ptr Urho.Texture2D <- getResource texname
    Urho.materialSetTexture pmat Urho.TUDiffuse diffTex
    Urho.materialSetTechnique pmat 0 diffTech Urho.MaterialQualityLow 0.0
  whenJust materialNormalMap $ \texname -> do
    diffNormalTech :: Ptr Urho.Technique <- getResource "Techniques/DiffNormal.xml"
    normTex :: Ptr Urho.Texture2D <- getResource texname
    Urho.materialSetTexture pmat Urho.TUNormal normTex
    Urho.materialSetTechnique pmat 1 diffNormalTech Urho.MaterialQualityMedium 0.0
  whenNothing ((,) <$> materialDiffuseTex <*> materialNormalMap) $ do
    noTexTech :: Ptr Urho.Technique <- getResource "Techniques/NoTexture.xml"
    Urho.materialSetTechnique pmat 0 noTexTech Urho.MaterialQualityLow 0.0
  Urho.materialSetShaderParameter pmat "MatDiffColor" materialDiffuseColor
  Urho.materialSetShaderParameter pmat "MatSpecColor" materialSpecularColor
  pure pmat
