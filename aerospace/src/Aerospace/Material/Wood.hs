module Aerospace.Material.Wood(
    woodMaterial
  ) where

import Aerospace.Color
import Aerospace.Material.Types

-- | Debug material that uses wood texture
woodMaterial :: Material
woodMaterial = Material {
    materialId            = "71911a84-8f2e-4011-8bd2-1bb7a6b4332f"
  , materialLocalId       = 0 -- set later
  , materialDiffuseTex    = Nothing --"Textures/Wood.png"
  , materialNormalMap     = Nothing --"Textures/WoodNormalmap.png"
  , materialPackedNormal  = False
  , materialDiffuseColor  = rgba (90/256) (56/256) (16/256) 1
  , materialSpecularColor = rgba 0.3 0.3 0.3 16
  }
