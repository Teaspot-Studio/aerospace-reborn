module Aerospace.Material.Skin(
    skinMaterial
  ) where

import Aerospace.Color
import Aerospace.Material.Types

-- | Debug material for body
skinMaterial :: Material
skinMaterial = Material {
    materialId            = "dce94221-62f4-4c45-906a-0832143a12ab"
  , materialLocalId       = 0 -- set later
  , materialDiffuseTex    = Nothing
  , materialNormalMap     = Nothing
  , materialPackedNormal  = False
  , materialDiffuseColor  = rgba (255/256) (220/256) (177/256) 1
  , materialSpecularColor = rgba 0.3 0.3 0.3 16
  }
