-- | Set of compile-time known materials
module Aerospace.Material.Core(
    coreMaterials
  , module R
  ) where

import Aerospace.Material.Skin as R
import Aerospace.Material.Stone as R
import Aerospace.Material.Types
import Aerospace.Material.Wood as R

import qualified Data.IntMap as IM
import qualified Data.Map.Strict as M

-- | Set of known materials for the core of the game
coreMaterials :: MaterialMap
coreMaterials = makeMaterialsMap [
    stoneMaterial
  , woodMaterial
  , skinMaterial
  ]

-- | Setup local ids from known list of materials
makeMaterialsMap :: [Material] -> MaterialMap
makeMaterialsMap ms = MaterialMap {
    materialLocalMap  = lm
  , materialGlobalMap = gm
  }
  where
    lm = IM.fromList $ fmap setupLocalId $ [0 .. ] `zip` ms
    lms = IM.elems lm
    gm = M.fromList $ fmap materialId lms `zip` lms
    setupLocalId (i, m) = (i, m { materialLocalId = i })
