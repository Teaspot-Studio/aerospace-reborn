module Aerospace.Material.Stone(
    stoneMaterial
  ) where

import Aerospace.Color
import Aerospace.Material.Types

-- | Debug material that uses stone texture
stoneMaterial :: Material
stoneMaterial = Material {
    materialId            = "3997fe10-a500-4220-832e-8fc53ea25f15"
  , materialLocalId       = 0 -- set later
  , materialDiffuseTex    = Nothing -- "Textures/StoneDiffuse.dds"
  , materialNormalMap     = Nothing -- "Textures/StoneNormal.dds"
  , materialPackedNormal  = True
  , materialDiffuseColor  = rgba (92/256) (90/256) (87/256) 1
  , materialSpecularColor = rgba 0.3 0.3 0.3 16
  }
