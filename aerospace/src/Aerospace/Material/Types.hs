module Aerospace.Material.Types(
    MaterialId
  , MaterialLocalId
  , Material(..)
  , MaterialMap(..)
  , lookupGlobalMaterial
  , lookupLocalMaterial
  ) where

import Aerospace.Color
import Data.IntMap (IntMap)
import Data.Map.Strict (Map)
import Data.Maybe
import Data.String
import Data.Text (Text)
import Data.UUID (UUID)
import GHC.Generics

import qualified Data.IntMap as IM
import qualified Data.Map.Strict as M
import qualified Data.UUID as UUID

-- | Material global unique id that must be unique across all mods.
type MaterialId = UUID

-- | Material id that is stored in mesh triangles.
-- Resolved dynamically at load time.
type MaterialLocalId = Int

-- | Material that is attached to solid models. Contains visual and physical properties.
data Material = Material {
-- | Material global unique id that must be unique across all mods.
  materialId            :: !MaterialId
-- | Material id that is stored in mesh triangles.
-- Resolved dynamically at load time.
, materialLocalId       :: !MaterialLocalId
-- | Path to diffuse texture
, materialDiffuseTex    :: !(Maybe Text)
-- | Path to normal map texture
, materialNormalMap     :: !(Maybe Text)
-- | If normals packed in xGxR (Y-component in the green channel, and X-component in the alpha),
-- then the flag should be 'True'
, materialPackedNormal  :: !Bool
-- | Diffuse color (main color)
, materialDiffuseColor  :: !Color
-- | Specular color (reflection of light)
, materialSpecularColor :: !Color
} deriving (Show, Generic)

-- | Mapping from id to material
data MaterialMap = MaterialMap {
  materialLocalMap  :: IntMap Material
, materialGlobalMap :: Map UUID Material
} deriving (Show, Generic)

-- | Find material by UUID
lookupGlobalMaterial :: MaterialId -> MaterialMap -> Maybe Material
lookupGlobalMaterial i MaterialMap{..} = M.lookup i materialGlobalMap

-- | Find material by local id
lookupLocalMaterial :: MaterialLocalId -> MaterialMap -> Maybe Material
lookupLocalMaterial i MaterialMap{..} = IM.lookup i materialLocalMap

instance IsString UUID where
  fromString = fromJust . UUID.fromString
