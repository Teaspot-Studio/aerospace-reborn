module Aerospace.Entity(
    EntityId(..)
  , Entity(..)
  , EntityH
  , EntityD
  , EntityE
  , joinEntity
  , switchEntity
  -- * Sync
  , entitySyncName
  , entitySyncFPS
  , entityPositionId
  , entityVelocityId
  , entityRotationId
  -- * Messages
  , entityCollectionId
  , EntityMessage(..)
  ) where

import Aerospace.Util
import Aerospace.Store()
import Data.Store
import Game.GoreAndAsh
import Game.GoreAndAsh.Sync
import GHC.Generics
import Graphics.Urho3D hiding (Event)

-- | Unique entity id
newtype EntityId = EntityId { unEntityId :: Int }
  deriving (Generic, Show, Eq, Ord)

instance Store EntityId

-- | Entity is game object that has its own model and independent position
-- in space.
--
-- Fields there are intentionally lazy as it need for MonadFix to work.
data Entity a b c = Entity {
-- | Position in world of entity
  entityPosition :: a
-- | Entity velocity that used for prediction
, entityVelocity :: b
-- | Pitch yaw roll of entity
, entityRotation :: c
}

type instance To f (Entity a b c) = Entity (f a) (f b) (f c)

-- | Entity with no wrapper around fields
type EntityH = Entity Vector3 Vector3 Vector3
-- | Dynamic fields for entity fields
type EntityD t = To (Dynamic t) EntityH
-- | Fields of entity that are wraped iinto events
type EntityE t = To (Event t) EntityH

-- | Merge dynamics layers for entity fields
joinEntity :: Reflex t => Dynamic t (EntityD t) -> EntityD t
joinEntity dp = Entity {
    entityPosition = join $ entityPosition <$> dp
  , entityVelocity = join $ entityVelocity <$> dp
  , entityRotation = join $ entityRotation <$> dp
  }

-- | Merge dynamics of events for entity fields
switchEntity :: Reflex t => Dynamic t (EntityE t) -> EntityE t
switchEntity dp = Entity {
    entityPosition = switch . current $ entityPosition <$> dp
  , entityVelocity = switch . current $ entityVelocity <$> dp
  , entityRotation = switch . current $ entityRotation <$> dp
  }

-- | Convert a speicif entity id to synchronization scope
entitySyncName :: EntityId -> SyncName
entitySyncName = show

-- | Maximum syncing packages per second for entity data
entitySyncFPS :: Int
entitySyncFPS = 32

-- | ID's for synchronization, they must be unique across synching object
entityPositionId, entityVelocityId, entityRotationId :: SyncItemId
entityPositionId = 0
entityVelocityId = 1
entityRotationId = 2

-- | ID for global message scope for entity collection
entityCollectionId :: SyncItemId
entityCollectionId = 1

-- | Network control messages for entities
data EntityMessage =
  -- | Allocated new entity in range of player
    NewEntityMessage !EntityId
  -- | Entity is removed from range of player
  | RemoveEntityMessage !EntityId
  -- | Client asks for entities for initial load
  | RequestEntitiesMessage
  -- | Response with serveral initial entities
  | ResponseEntitiesMessage [EntityId]
  deriving (Generic, Show)

instance Store EntityMessage
