module Aerospace.Vector3(
    module Graphics.Urho3D.Math.Vector3
  , module Aerospace.Vector
  , crossv
  , signedAngle
  , eulerToDirection
  ) where

import Aerospace.Vector
import Graphics.Urho3D.Math.Quaternion
import Graphics.Urho3D.Math.Vector3

type instance VectorElement Vector3 = Float

instance ScaleVector Vector3 where
  scalev (Vector3 vx vy vz) v = Vector3 (vx*v) (vy*v) (vz*v)
  {-# INLINE scalev #-}

instance LengthVector Vector3 where
  lengthv = vec3Length
  {-# INLINE lengthv #-}

instance DotVector Vector3 where
  dotv = vec3Dot
  {-# INLINE dotv #-}

instance NormalizeVector Vector3
instance LerpVector Vector3

-- | Cross product of vectors
crossv :: Vector3 -> Vector3 -> Vector3
crossv = vec3Cross
{-# INLINE crossv #-}

-- | Calculate signed angle between vectors in radians.
signedAngle :: Vector3 -> Vector3 -> Vector3 -> Float
signedAngle upV a b = let
  angle = acos $ normalizev a `dotv` normalizev b
  n = crossv a b
  s = if dotv upV n > 0 then 1 else (-1)
  in s * angle

-- | Convert euler angles to vector direction that coresponds to the angles
--
-- Angles are graduses
eulerToDirection :: Vector3 -> Vector3
eulerToDirection (Vector3 pa ya ra) = quaternionRotate (quaternionFromEuler pa ya ra) (Vector3 1 0 0)
