module Aerospace.Store(

  ) where

import Data.Store
import Graphics.Urho3D

instance Store Vector4
instance Store Vector3
instance Store Vector2
instance Store IntVector2
