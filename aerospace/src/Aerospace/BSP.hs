-- | Binary space partitioning operations and types
--
-- Code is inspired by csg.js library (Evan Wallace (http://madebyevan.com/))
{-# OPTIONS_GHC -fno-warn-unused-local-binds #-}
module Aerospace.BSP(
  -- * Types
    BSPNode(..)
  -- * Creation of nodes
  , bspNode
  , bspRebuild
  , bspPrimitive
  -- * Getting results
  , bspTriangles
  , bspTrianglesMap
  -- * Boolean operations
  , bspInvert
  , bspUnion
  , bspSubtract
  , bspIntersect
  -- * Space transformations
  , bspRotate
  , bspTranslate
  , bspScale
  -- * Low level clipping
  , bpsClipTriangles
  , bspClipTo
  ) where

import Aerospace.Plane
import Aerospace.Quaternion
import Aerospace.Triangle
import Aerospace.Vector3
import Control.DeepSeq
import Data.Map.Strict (Map)
import Data.Maybe
import Data.Vector.Unboxed (Vector, Unbox)
import GHC.Generics

import qualified Data.Map.Strict as M
import qualified Data.Vector.Unboxed as V

-- | BSP tree built from 3D mesh. A BSP tree is built from a collection of triangles
-- by picking a triangle to split along. That triangle (and all other coplanar
-- triangles) are added directly to that node and the other triangles are added to
-- the front and/or back subtrees. This is not a leafy BSP tree since there is
-- no distinction between internal and leaf nodes.
data BSPNode a = BSPNode {
-- | Separation plane
  bspNodePlane    :: {-# UNPACK #-} !Plane
-- | Front part of space
, bspNodeFront    :: !(Maybe (BSPNode a))
-- | Back paret of space
, bspNodeBack     :: !(Maybe (BSPNode a))
-- | Collection of triangles for the given BSP
, bspNodeTriangles :: !(Vector (VertexTriangle a))
} deriving (Show, Generic)

instance NFData a => NFData (BSPNode a)

-- | Shortcut for monotuple
type V2 a = (a, a)
-- | Shortcut for monotuple
type V3 a = (a, a, a)

-- | Build a BSP tree out of `triangles`. When called on an existing tree, the
-- new triangles are filtered down to the bottom of the tree and become new
-- nodes there. Each set of triangles is partitioned using the first triangle
-- (no heuristic is used to pick a good split).
bspNode :: forall a . Unbox a => Maybe (BSPNode a) -> Vector (VertexTriangle a) -> Maybe (BSPNode a)
bspNode bsp vs
  | V.null vs = bsp
  | otherwise = Just BSPNode {
      bspNodePlane      = p
    , bspNodeFront      = bspNode (bspNodeFront =<< bsp) frontVs
    , bspNodeBack       = bspNode (bspNodeBack =<< bsp) backVs
    , bspNodeTriangles  = thisVs
    }
    where
      p = maybe (planeFromTriangle $ V.head vs) bspNodePlane bsp
      (thisVs, frontVs, backVs) = V.foldl' split (maybe V.empty bspNodeTriangles bsp, V.empty, V.empty) vs

      split :: V3 (Vector (VertexTriangle a)) -> VertexTriangle a -> V3 (Vector (VertexTriangle a))
      split (!coVs, !fVs, !bVs) v = case splitTriangle p v of
        SplitCoplonarFront -> (V.snoc coVs v, fVs, bVs)
        SplitCoplonarBack -> (V.snoc coVs v, fVs, bVs)
        SplitFront -> (coVs, V.snoc fVs v, bVs)
        SplitBack -> (coVs, fVs, V.snoc bVs v)
        SplitSpanning fs bs -> (coVs, fVs V.++ fs, bVs V.++ bs)
{-# INLINABLE bspNode #-}

-- | Same as 'bspNode' but rebuilds the given bsp node with new additional data.
bspRebuild :: forall a . Unbox a => BSPNode a -> Vector (VertexTriangle a) -> BSPNode a
bspRebuild bsp = fromMaybe bsp . bspNode (Just bsp)
{-# INLINE bspRebuild #-}

-- | Same as 'bspNode' but creates bsp from scratch for the given triangles.
-- Fails with `error` with input triangles are empty (must be impossible for primitives).
bspPrimitive :: forall a . Unbox a => Vector (VertexTriangle a) -> BSPNode a
bspPrimitive vs = case bspNode Nothing vs of
  Nothing -> error "bspPrimitive: impossible, empty input of triangles!"
  Just bsp -> bsp

-- | Get all triangles stored in BSP node
bspTriangles :: forall a . Unbox a => BSPNode a -> Vector (VertexTriangle a)
bspTriangles BSPNode{..} = bspNodeTriangles V.++ front V.++ back
  where
    front = maybe V.empty bspTriangles bspNodeFront
    back = maybe V.empty bspTriangles bspNodeBack
{-# INLINE bspTriangles #-}

-- | Get all triangles stored in BSP node splitted by custom value
bspTrianglesMap :: forall a . (Unbox a, Eq a, Ord a) => BSPNode a -> Map a (Vector (VertexTriangle a))
bspTrianglesMap bsp = V.foldl' collect mempty $ bspTriangles bsp
  where
    collect :: Map a (Vector (VertexTriangle a)) -> VertexTriangle a -> Map a (Vector (VertexTriangle a))
    collect !acc tr = case M.lookup k acc of
      Nothing -> M.insert k (V.singleton tr) acc
      Just vs -> M.insert k (V.snoc vs tr) acc
      where k = vtriangleCustom tr

-- | Recursively remove all polygons in `polygons` that are inside this BSP tree.
bpsClipTriangles :: forall a . Unbox a => BSPNode a -> Vector (VertexTriangle a) -> Vector (VertexTriangle a)
bpsClipTriangles BSPNode{..} vs = front V.++ back
  where
    front = maybe frontVs (`bpsClipTriangles` frontVs) bspNodeFront
    back = maybe V.empty (`bpsClipTriangles` backVs) bspNodeBack
    (frontVs, backVs) = V.foldl' split (V.empty, V.empty) vs

    split :: V2 (Vector (VertexTriangle a)) -> VertexTriangle a -> V2 (Vector (VertexTriangle a))
    split (!fVs, !bVs) v = case splitTriangle bspNodePlane v of
      SplitCoplonarFront -> (V.snoc fVs v, bVs)
      SplitCoplonarBack -> (fVs, V.snoc bVs v)
      SplitFront -> (V.snoc fVs v, bVs)
      SplitBack -> (fVs, V.snoc bVs v)
      SplitSpanning fs bs -> (fVs V.++ fs, bVs V.++ bs)
{-# INLINABLE bpsClipTriangles #-}

-- | Remove all polygons in this BSP tree that are inside the other BSP tree
bspClipTo :: forall a . Unbox a => BSPNode a -> BSPNode a -> BSPNode a
bspClipTo a b = a {
    bspNodeFront     = (`bspClipTo` b) <$> bspNodeFront a
  , bspNodeBack      = (`bspClipTo` b) <$> bspNodeBack a
  , bspNodeTriangles = bpsClipTriangles b (bspNodeTriangles a)
  }
{-# INLINE bspClipTo #-}

-- | Convert solid space to empty space and empty space to solid space.
bspInvert :: forall a . Unbox a => BSPNode a -> BSPNode a
bspInvert bsp = BSPNode {
    bspNodePlane = planeFlip $ bspNodePlane bsp
  , bspNodeFront = bspInvert <$> bspNodeBack bsp
  , bspNodeBack = bspInvert <$> bspNodeFront bsp
  , bspNodeTriangles = V.map triangleFlip $ bspNodeTriangles bsp
  }
{-# INLINE bspInvert #-}

-- | Return a new BSP solid representing space in either `a` solid or in the
-- solid `b`.
-- @@
--     bspUnion a b
--
--     +-------+            +-------+
--     |       |            |       |
--     |   a   |            |       |
--     |    +--+----+   =   |       +----+
--     +----+--+    |       +----+       |
--          |   b   |            |       |
--          |       |            |       |
--          +-------+            +-------+
-- @@
bspUnion :: forall a . Unbox a => BSPNode a -> BSPNode a -> BSPNode a
bspUnion a b = a' -- bspRebuild b (bspTriangles $ a `bspClipTo` b)
  where
    ab = a `bspClipTo` b
    ba = b `bspClipTo` ab
    b' = bspInvert $ (bspInvert ba) `bspClipTo` a
    a' = bspRebuild ab (bspTriangles b')
{-# INLINE bspUnion #-}

-- | Return a new BSP solid representing space in `a` solid but not in the
-- solid `b`.
-- @@
--     bspSubtract a b
--
--     +-------+            +-------+
--     |       |            |       |
--     |   a   |            |       |
--     |    +--+----+   =   |    +--+
--     +----+--+    |       +----+
--          |   b   |
--          |       |
--          +-------+
-- @@
bspSubtract :: forall a . Unbox a => BSPNode a -> BSPNode a -> BSPNode a
bspSubtract a b = a'
  where
    ab = bspInvert a `bspClipTo` b
    ba = bspInvert $ b `bspClipTo` ab
    b' = bspInvert $ ba `bspClipTo` ab
    a' = bspInvert $ bspRebuild ab (bspTriangles b')
{-# INLINE bspSubtract #-}

-- | Return a new BSP solid representing space both `a` solid and in the
-- solid `b`.
-- @@
--     bspIntersect a b
--
--     +-------+
--     |       |
--     |   a   |
--     |    +--+----+   =   +--+
--     +----+--+    |       +--+
--          |   b   |
--          |       |
--          +-------+
-- @@
bspIntersect :: forall a . Unbox a => BSPNode a -> BSPNode a -> BSPNode a
bspIntersect a b = a'
  where
    ai = bspInvert a
    ba = bspInvert $ b `bspClipTo` ai
    ab = ai `bspClipTo` ba
    b' = ba `bspClipTo` ab
    a' = bspInvert $ bspRebuild ab (bspTriangles b')
{-# INLINE bspIntersect #-}

-- | Transform all triangles inside the BSP tree
bspMapTriangles :: forall a b . (Unbox a, Unbox b) => (VertexTriangle a -> VertexTriangle b) -> BSPNode a -> BSPNode b
bspMapTriangles f bsp = bsp {
    bspNodeFront      = bspMapTriangles f <$> bspNodeFront bsp
  , bspNodeBack       = bspMapTriangles f <$> bspNodeBack bsp
  , bspNodeTriangles  = newTriangles
  , bspNodePlane      = if V.null (bspNodeTriangles bsp) then bspNodePlane bsp else planeFromTriangle $ V.head newTriangles
  }
  where newTriangles = V.map f $ bspNodeTriangles bsp
{-# INLINE bspMapTriangles #-}

-- | Rotate all triangles with given quaternion
bspRotate :: forall a . Unbox a => Quaternion -> BSPNode a -> BSPNode a
bspRotate q = bspMapTriangles (triangleRotate q)
{-# INLINE bspRotate #-}

-- | Move by given vector all triangles
bspTranslate :: forall a . Unbox a => Vector3 -> BSPNode a -> BSPNode a
bspTranslate t = bspMapTriangles (triangleTranslate t)
{-# INLINE bspTranslate #-}

-- | Scale by the given vector all triangles
bspScale :: forall a . Unbox a => Vector3 -> BSPNode a -> BSPNode a
bspScale t = bspMapTriangles (triangleScale t)
{-# INLINE bspScale #-}
