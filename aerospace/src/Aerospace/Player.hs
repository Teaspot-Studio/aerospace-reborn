module Aerospace.Player(
    PlayerId(..)
  , Player(..)
  , PlayerH
  , PlayerD
  , joinPlayer
  , PlayerE
  , switchPlayer
  , PlayerMessage(..)
  , playerSyncName
  , playerSyncFPS
  , playerGlobalId
  , playerPositionId
  , playerVelocityId
  , playerRotationId
  ) where

import Aerospace.Entity
import Aerospace.Store()
import Aerospace.Util
import Data.Store
import Game.GoreAndAsh
import Game.GoreAndAsh.Sync
import GHC.Generics
import Graphics.Urho3D hiding (Event)

-- | Unique player id
newtype PlayerId = PlayerId { unPlayerId :: Int }
  deriving (Generic, Show, Eq, Ord)

instance Store PlayerId

-- | Player info that is client side synched to the server (and checked for the
-- cheating on server).
--
-- Fields there are intentionally lazy as it need for MonadFix to work.
data Player a b c = Player {
-- | Position in world of entity
  playerPosition :: a
-- | Entity velocity that used for prediction
, playerVelocity :: b
-- | Pitch yaw roll of player
, playerRotation :: c
}

type instance To f (Player a b c) = Player (f a) (f b) (f c)

-- | Player with no wrapper around fields
type PlayerH = Player Vector3 Vector3 Vector3
-- | Dynamic fields for player fields
type PlayerD t = To (Dynamic t) PlayerH
-- | Fields of player that are wraped iinto events
type PlayerE t = To (Event t) PlayerH

-- | Merge dynamics layers for player fields
joinPlayer :: Reflex t => Dynamic t (PlayerD t) -> PlayerD t
joinPlayer dp = Player {
    playerPosition = join $ playerPosition <$> dp
  , playerVelocity = join $ playerVelocity <$> dp
  , playerRotation = join $ playerRotation <$> dp
  }

-- | Merge dynamics of events for player fields
switchPlayer :: Reflex t => Dynamic t (PlayerE t) -> PlayerE t
switchPlayer dp = Player {
    playerPosition = switch . current $ playerPosition <$> dp
  , playerVelocity = switch . current $ playerVelocity <$> dp
  , playerRotation = switch . current $ playerRotation <$> dp
  }

-- | Convert a speicif player id to synchronization scope
playerSyncName :: PlayerId -> SyncName
playerSyncName = show

-- | Maximum syncing packages per second for player data
playerSyncFPS :: Int
playerSyncFPS = 32

-- | ID's for synchronization, they must be unique across synching object
playerPositionId, playerVelocityId, playerRotationId :: SyncItemId
playerPositionId = 0
playerVelocityId = 1
playerRotationId = 2

-- | ID for global message scope for player
playerGlobalId :: SyncItemId
playerGlobalId = 2

-- | Network control messages for player
data PlayerMessage =
  -- | Client asks player id and entity id
    ReqSelfPlayerId
  -- | Response from server about player id and entity id
  | RespSelfPlayerId !PlayerId !EntityId
  deriving (Generic, Show)

instance Store PlayerMessage
