module Aerospace.Util(
    showt
  , whenNothing
  , whenJust
  , guardNothing
  , To
  ) where

import Data.Text (Text, pack)

-- | Allows you to write To Event Player instead of Player (Event a) (Event b) [..]
type family To (a :: * -> *) (b :: *) :: *

-- | Print in text any value that has 'Show'
showt :: Show a => a -> Text
showt = pack . show

-- | Apply action only when the first parameter is 'Nothing'
whenNothing :: Applicative f => Maybe a -> f () -> f ()
whenNothing (Just _) _ = pure ()
whenNothing Nothing m = m

-- | Helper function that passes through only 'Just' values and fails otherwise
guardNothing :: Monad m => String -> Maybe a -> m a
guardNothing msg Nothing = fail $ "guardNothing: " ++ msg
guardNothing _ (Just a) = pure a

-- | Apply action only when the first parameter is 'Just'
whenJust :: Applicative f => Maybe a -> (a -> f ()) -> f ()
whenJust (Just v) m = m v
whenJust Nothing _ = pure ()
