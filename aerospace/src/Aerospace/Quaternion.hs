module Aerospace.Quaternion(
    module Graphics.Urho3D.Math.Quaternion
  , zeroRotation
  , quaternionFromAxis
  ) where

import Aerospace.Vector3 
import Data.Vector.Unboxed (Unbox)
import Data.Vector.Unboxed.Deriving
import Graphics.Urho3D.Math.Quaternion

import qualified Data.Vector.Unboxed as V

derivingUnbox "Quaternion"
    [t| Quaternion -> (Float, Float, Float, Float) |]
    [| \Quaternion{..} -> (_quaternionX, _quaternionY, _quaternionZ, _quaternionW) |]
    [| \(vx, vy, vz, vw) -> Quaternion vx vy vz vw |]

-- | Quaternion that encodes no rotation
zeroRotation :: Quaternion
zeroRotation = quaternionFromEuler 0 0 0

-- | Describe rotation by axis and angle (radians)
quaternionFromAxis :: Vector3 -> Float -> Quaternion
quaternionFromAxis (Vector3 ax ay az) ang = Quaternion
  (ax * sin (ang / 2))
  (ay * sin (ang / 2))
  (az * sin (ang / 2))
  (cos (ang / 2))
