{-# LANGUAGE DefaultSignatures #-}
-- | Generic operations on vectors
module Aerospace.Vector(
    VectorElement
  , ScaleVector(..)
  , LengthVector(..)
  , DotVector(..)
  , NormalizeVector(..)
  , LerpVector(..)
  ) where

import Graphics.Urho3D.Math

type family VectorElement a

class ScaleVector a where
  scalev :: a -> VectorElement a -> a
{-# SPECIALIZE INLINE scalev :: Vector3 -> Float -> Vector3 #-}
{-# SPECIALIZE INLINE scalev :: Vector2 -> Float -> Vector2 #-}

class LengthVector a where
  lengthv :: a -> VectorElement a
{-# SPECIALIZE INLINE lengthv :: Vector3 -> Float #-}
{-# SPECIALIZE INLINE lengthv :: Vector2 -> Float #-}

class DotVector a where
  dotv :: a -> a -> VectorElement a
{-# SPECIALIZE INLINE dotv :: Vector3 -> Vector3 -> Float #-}
{-# SPECIALIZE INLINE dotv :: Vector2 -> Vector2 -> Float #-}

class NormalizeVector a where
  normalizev :: a -> a
  default normalizev :: (LengthVector a, ScaleVector a, Fractional (VectorElement a)) => a -> a
  normalizev a = a `scalev` recip (lengthv a)
{-# SPECIALIZE INLINE normalizev :: Vector3 -> Vector3 #-}
{-# SPECIALIZE INLINE normalizev :: Vector2 -> Vector2 #-}

class LerpVector a where
  -- | Given `lerp t a b` interpolates linearly new c `c = a + (b-a)*t`
  lerpv :: VectorElement a -> a -> a -> a
  default lerpv :: (ScaleVector a, Num a) => VectorElement a -> a -> a -> a
  lerpv t a b = a + scalev (b - a) t
{-# SPECIALIZE INLINE lerpv :: Vector3 -> Vector3 #-}
{-# SPECIALIZE INLINE lerpv :: Vector2 -> Vector2 #-}
