-- | Defines types to work with polygons
module Aerospace.Polygon(
    Polygon(..)
  , polygonSegments
  , polygonCentroid
  , polygonTriangles
  , circlePolygon
  , rotatePolygon
  , translatePolygon
  , scalePolygon
  ) where

import Aerospace.Quaternion
import Aerospace.Vector3
import Control.DeepSeq
import Data.Vector.Unboxed (Vector)
import GHC.Generics

import qualified Data.Vector.Unboxed as V

-- | Defines planar closed figure from segments
newtype Polygon = Polygon { unPolygon :: Vector Vector3 }
  deriving (Show, Generic)

instance NFData Polygon

-- | Return line segments for the polygon
polygonSegments :: Polygon -> [(Vector3, Vector3)]
polygonSegments (Polygon vs)
  | V.null vs = []
  | otherwise = go vs
  where
    startv = V.head vs
    go xs
      | V.null xs = []
      | V.length xs == 1 = [(V.head xs, startv)]
      | otherwise = (V.head xs, xs V.! 1) : go (V.drop 1 xs)

-- | Return average of all polygon points
polygonCentroid :: Polygon -> Vector3
polygonCentroid (Polygon vs)
  | V.null vs = Vector3 0 0 0
  | otherwise = V.sum vs `scalev` (recip . fromIntegral . V.length $ vs)

-- | Triangluate polygon with simple fan algorithm
polygonTriangles :: Polygon -> [(Vector3, Vector3, Vector3)]
polygonTriangles (Polygon vs)
  | V.length vs < 3 = []
  | otherwise = go $ V.drop 1 vs
  where
    v1 = V.head vs
    go xs
      | V.length xs < 2 = []
      | otherwise = (v1, V.head xs, xs V.! 1) : go (V.drop 1 xs)

-- | Generate polygon in XZ plane that is close to circle with given amount of points. Center in origin and radius equals 1
circlePolygon :: Int -> Polygon
circlePolygon n = Polygon . V.fromList . reverse $ mkPoint <$> [0 .. n - 1]
  where
    dang = 2 * pi / fromIntegral n
    mkPoint i = let a = fromIntegral i * dang in Vector3 (cos a) 0 (sin a)

-- | Rotate polygon by the quaternion
rotatePolygon :: Quaternion -> Polygon -> Polygon
rotatePolygon q (Polygon xs) = Polygon $ V.map (quaternionRotate q) xs

-- | Apply offset to the polygon
translatePolygon :: Vector3 -> Polygon -> Polygon
translatePolygon v (Polygon xs) = Polygon $ V.map (+ v) xs

-- | Apply scale to the polygon
scalePolygon :: Vector3 -> Polygon -> Polygon
scalePolygon sv (Polygon xs) = Polygon $ V.map (* sv) xs
