module Aerospace.Vertex(
    Vertex(..)
  , vertexFlip
  , vertexRotate
  , vertexTranslate
  , vertexScale
  , vertexNormalize
  , interpolateVertex
  ) where

import Aerospace.Quaternion
import Aerospace.Vector2
import Aerospace.Vector3
import Control.DeepSeq
import Data.Vector.Unboxed
import Data.Vector.Unboxed.Deriving
import GHC.Generics

-- | Vertex of a 3D mesh with additional attributes
data Vertex = Vertex {
-- | Vertex position in space
  vertexPosition :: {-# UNPACK #-} !Vector3
-- | Vertex normal
, vertexNormal   :: {-# UNPACK #-} !Vector3
-- | Vertex UV coordinates
, vertexTexCoord :: {-# UNPACK #-} !Vector2
} deriving (Show, Generic)

instance NFData Vertex

derivingUnbox "VertexTriangle"
    [t| Vertex -> (Vector3, Vector3, Vector2) |]
    [| \Vertex{..} -> (vertexPosition, vertexNormal, vertexTexCoord) |]
    [| \(v1, v2, v3) -> Vertex v1 v2 v3 |]

-- | Flip orientation dependent data (normal)
vertexFlip :: Vertex -> Vertex
vertexFlip v = v { vertexNormal = negate $ vertexNormal v }
{-# INLINE vertexFlip #-}

-- | Rotate vertex by given quaternion
vertexRotate :: Quaternion -> Vertex -> Vertex
vertexRotate q v = v {
    vertexPosition  = quaternionRotate q $ vertexPosition v
  , vertexNormal    = quaternionRotate q $ vertexNormal v
  }
{-# INLINE vertexRotate #-}

-- | Translate vertex by given vector offset
vertexTranslate :: Vector3 -> Vertex -> Vertex
vertexTranslate t v = v {
    vertexPosition  = t + vertexPosition v
  }
{-# INLINE vertexTranslate #-}

-- | Scale vertex by given vector
vertexScale :: Vector3 -> Vertex -> Vertex
vertexScale t v = v {
    vertexPosition  = t * vertexPosition v
  , vertexNormal    = normalizev $ t * vertexNormal v
  }
{-# INLINE vertexScale #-}

-- | Make distance from origin equals 1 for the Vertex
vertexNormalize :: Vertex -> Vertex
vertexNormalize v = v {
    vertexPosition = normalizev $ vertexPosition v
  }
{-# INLINE vertexNormalize #-}

-- | Linearly interpolate vertex and other vertex based on float number (0 to 1)
interpolateVertex :: Float -> Vertex -> Vertex -> Vertex
interpolateVertex t a b = Vertex {
    vertexPosition  = lerpv t (vertexPosition a) (vertexPosition b)
  , vertexNormal    = lerpv t (vertexNormal a) (vertexNormal b)
  , vertexTexCoord  = lerpv t (vertexTexCoord a) (vertexTexCoord b)
  }
{-# INLINE interpolateVertex #-}
