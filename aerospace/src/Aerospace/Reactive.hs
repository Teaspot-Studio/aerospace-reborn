module Aerospace.Reactive(
    EventWithTrigger
  , newReactiveMVar
  , holdDynMap
  , networkHoldE
  , networkHoldE2
  , refireEvents
  , differentiate
  , differentiateWithReset
  ) where

import Control.Concurrent
import Control.Monad
import Control.Monad.Fix
import Control.Monad.IO.Class
import Data.Foldable (traverse_)
import Data.Map.Strict (Map)
import Data.Time.Clock.POSIX
import Game.GoreAndAsh.Core
import Game.GoreAndAsh.Sync
import Reflex
import Reflex.Network

import qualified Data.Map.Strict as M

-- | Shortcut for event alongside with trigger
type EventWithTrigger t a = (Event t a, a -> IO ())

-- | Create empty mvar that fires and event when finished
newReactiveMVar :: (MonadIO m, TriggerEvent t m) => m (MVar a, Event t a)
newReactiveMVar = do
  (e, fire) <- newTriggerEvent
  mvar <- liftIO newEmptyMVar
  void . liftIO . forkIO $ fire =<< takeMVar mvar
  pure (mvar, e)

-- | Waits for occurence of values and accumulate it into dynamic map
holdDynMap :: forall t m k v . (MonadHold t m, MonadFix m, Reflex t, Ord k)
  => Dynamic t (Map k (Event t v)) -> m (Dynamic t (Map k v))
holdDynMap dem = do
  let dem' :: Dynamic t (Event t (Map k v))
      dem' = mergeMap <$> dem
  foldDyn M.union mempty $ switch . current $ dem'

-- | Wrapper around 'networkHold' that automatically switches resulted event
networkHoldE :: forall t m a . (Adjustable t m, MonadHold t m) => Event t (m (Event t a)) -> m (Event t a)
networkHoldE = fmap (switch . current) . networkHold (pure never)

-- | Wrapper around 'networkHold' that automatically switches resulted event
networkHoldE2 :: forall t m a b . (Adjustable t m, MonadHold t m) => Event t (m (Event t a, Event t b)) -> m (Event t a, Event t b)
networkHoldE2 e = do
  res <- networkHold (pure (never, never)) e
  pure (switch . current $ fst <$> res, switch . current $ snd <$> res)

-- | Collect events into internal buffer, resend them again when got fire of second event. Outputs
-- the same occurences the first event provides. Accumulation works only once.
--
-- Main use case is to not skip messages from peers when we waiting initialization of some huge resource.
refireEvents :: forall t m a . (MonadHold t m, MonadIO m, TriggerEvent t m, MonadIO (Performable m), PerformEvent t m)
  => Event t a -- ^ Original event, occurences that we collect
  -> Event t () -- ^ One time trigger to refire all occurences before the trigger.
  -> m (Event t a) -- ^ Also fires all occurences of original event
refireEvents e refireE = do
  ref <- newExternalRef (Just [])
  performEvent_ $ ffor e $ \v -> modifyExternalRef ref (\case
    Just acc -> (Just $ v : acc, ())
    Nothing -> (Nothing, ()))
  (resE, fire) <- newTriggerEvent
  refireE' <- headE refireE
  performEvent_ $ ffor refireE' $ const $ do
    mres <- readExternalRef ref
    liftIO $ traverse_ (traverse_ fire) mres
    writeExternalRef ref Nothing
  performEvent_ $ ffor e $ \v -> liftIO $ fire v
  pure resE

-- | Calculate velocity of changing value
differentiate :: forall t m a . (MonadIO (Performable m), MonadIO m, PerformEvent t m, MonadHold t m, TriggerEvent t m, Fractional a)
  => Dynamic t a
  -> m (Dynamic t a)
differentiate dv = do
  initV <- sample . current $ dv
  initClock <- liftIO getPOSIXTime
  oldRef <- newExternalRef (initV, initClock)
  velE <- performEvent $ ffor (updated dv) $ \curV -> do
    curClock <- liftIO getPOSIXTime
    (oldV, oldClock) <- readExternalRef oldRef
    writeExternalRef oldRef (curV, curClock)
    pure $ (curV - oldV) / realToFrac (curClock - oldClock)
  holdDyn 0 velE

-- | Calculate velocity of changing value
differentiateWithReset :: forall t m a . (MonadIO (Performable m), MonadIO m, PerformEvent t m, MonadHold t m, TriggerEvent t m, Fractional a)
  => Dynamic t a -- ^ When updates, replaces resulted value
  -> Dynamic t a
  -> m (Dynamic t a)
differentiateWithReset rv dv = do
  initV <- sample . current $ dv
  initClock <- liftIO getPOSIXTime
  oldRef <- newExternalRef (initV, initClock)
  velE <- performEvent $ ffor (updated dv) $ \curV -> do
    curClock <- liftIO getPOSIXTime
    (oldV, oldClock) <- readExternalRef oldRef
    writeExternalRef oldRef (curV, curClock)
    pure $ (curV - oldV) / realToFrac (curClock - oldClock)
  initRVal <- sample . current $ rv
  holdDyn initRVal $ leftmost [updated rv, velE]
