{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
-- | Define triangle that consists of vertex data for model rendering
module Aerospace.Triangle(
    VertexTriangle(..)
  , triangleFlip
  , triangleNormal
  , triangleRotate
  , triangleTranslate
  , triangleScale
  , combineTriangles
  ) where

import Aerospace.Quaternion
import Aerospace.Vector3
import Aerospace.Vertex
import Control.DeepSeq
import Data.Vector.Unboxed (Unbox)
import Data.Vector.Unboxed.Deriving
import GHC.Generics

import qualified Data.Vector.Unboxed as V

data VertexTriangle a = VertexTriangle {
-- | Triangle first point
  vtriangleFirst   :: {-# UNPACK #-} !Vertex
-- | Triangle second point
, vtriangleSecond  :: {-# UNPACK #-} !Vertex
-- | Triangle third point
, vtriangleThird   :: {-# UNPACK #-} !Vertex
-- | Triangle additional data
, vtriangleCustom  :: !a
} deriving (Show, Generic, Functor, Foldable, Traversable)

instance NFData a => NFData (VertexTriangle a)

derivingUnbox "VertexTriangle"
    [t| forall a . Unbox a => VertexTriangle a -> (Vertex, Vertex, Vertex, a) |]
    [| \VertexTriangle{..} -> (vtriangleFirst, vtriangleSecond, vtriangleThird, vtriangleCustom) |]
    [| \(v1, v2, v3, v4) -> VertexTriangle v1 v2 v3 v4 |]

-- | Changing directional info in vertecies into opposite
triangleFlip :: VertexTriangle a -> VertexTriangle a
triangleFlip tr = tr {
    vtriangleFirst  = vertexFlip $ vtriangleFirst tr
  , vtriangleSecond = vertexFlip $ vtriangleThird tr
  , vtriangleThird  = vertexFlip $ vtriangleSecond tr
  }
{-# INLINE triangleFlip #-}

-- | Calculate normal of the triangle using only positions
triangleNormal :: VertexTriangle a -> Vector3
triangleNormal VertexTriangle{..} = normalizev $ (b - a) `crossv` (c - a)
  where
    a = vertexPosition vtriangleFirst
    b = vertexPosition vtriangleSecond
    c = vertexPosition vtriangleThird
{-# INLINE triangleNormal #-}

-- | Rotate vertecies of the triangle by given quaternion
triangleRotate :: Quaternion -> VertexTriangle a -> VertexTriangle a
triangleRotate q tr = tr {
    vtriangleFirst  = vertexRotate q $ vtriangleFirst tr
  , vtriangleSecond = vertexRotate q $ vtriangleSecond tr
  , vtriangleThird  = vertexRotate q $ vtriangleThird tr
  }
{-# INLINE triangleRotate #-}

-- | Translate vertecies of the triangle by given offset
triangleTranslate :: Vector3 -> VertexTriangle a -> VertexTriangle a
triangleTranslate v tr = tr {
    vtriangleFirst  = vertexTranslate v $ vtriangleFirst tr
  , vtriangleSecond = vertexTranslate v $ vtriangleSecond tr
  , vtriangleThird  = vertexTranslate v $ vtriangleThird tr
  }
{-# INLINE triangleTranslate #-}

-- | Scale vertecies of the triangle by given vector
triangleScale :: Vector3 -> VertexTriangle a -> VertexTriangle a
triangleScale v tr = tr {
    vtriangleFirst  = vertexScale v $ vtriangleFirst tr
  , vtriangleSecond = vertexScale v $ vtriangleSecond tr
  , vtriangleThird  = vertexScale v $ vtriangleThird tr
  }
{-# INLINE triangleScale #-}

-- | Collect vertecies from array and combine triangles (if not sufficient points, skips them)
combineTriangles :: Unbox a => a -> V.Vector Vertex -> V.Vector (VertexTriangle a)
combineTriangles !a vs
  | V.length vs < 3 = V.empty
  | otherwise = tr `V.cons` combineTriangles a (V.drop 3 vs)
  where
    v1 = vs V.! 0
    v2 = vs V.! 1
    v3 = vs V.! 2
    tr = VertexTriangle v1 v2 v3 a
