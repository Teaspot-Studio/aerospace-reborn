module Aerospace.CSG.Triangle(
    bspTrianglePrimitive
  , rescaleVertecies
  , prettyPrintVerts
  ) where

import Aerospace.Triangle
import Aerospace.Vector3
import Aerospace.Vertex
import Data.Foldable (foldr')
import Data.Vector.Unboxed (Vector, Unbox)

import qualified Data.Vector.Unboxed as V

-- | Primitive that contains a single triangle. Used for debugging only.
bspTrianglePrimitive :: forall a . Unbox a => a -> Vector (VertexTriangle a)
bspTrianglePrimitive a = V.fromList [tr1]
  where
    v1 = Vector3 0 0 0
    v2 = Vector3 1 0 0
    v3 = Vector3 0.5 1 0
    n = Vector3 0 0 1
    tr1 = VertexTriangle (Vertex v1 n 0) (Vertex v3 n 0) (Vertex v2 n 0) a

-- | Allows to adjust bounding box of vertecies
-- Used as development helper in gchi for rescaling primitives.
rescaleVertecies :: Float -> Vector Vector3 -> Vector Vector3
rescaleVertecies v vs = V.map (Vector3 v v v *) vs

-- | Output vertecies comfy for copy-paste in code
prettyPrintVerts :: Vector Vector3 -> String
prettyPrintVerts vs = foldr' collect "" $ V.toList vs
  where
    collect (Vector3 vx vy vz) !acc = "Vector3 " ++ shown vx ++ " " ++ shown vy ++ " " ++ shown vz ++ "\n" ++ acc
    shown v = if v < 0 then "(" ++ show v ++ ")" else show v
