-- | Defines primitive for extruding basic shapes
module Aerospace.CSG.Sphere(
    spherePrimitive
  ) where

import Aerospace.CSG.Polyhedron
import Aerospace.Triangle
import Aerospace.Vector3
import Aerospace.Vertex
import Data.Vector.Unboxed (Vector, Unbox)

import qualified Data.Vector.Unboxed as V

-- | Sphere with center at origin and radius equals 1. Int parameter refers to
-- sphere approximation quality (used icosahedron algorithm), the higher, the
-- more triangles are produced.
spherePrimitive :: forall a . Unbox a => Int -> a -> Vector (VertexTriangle a)
spherePrimitive steps a = V.fromList $ go 0 . V.toList $ icosahedronPrimitive a
  where
    go n vs
      | n >= steps = vs
      | otherwise = go (n+1) $ concatMap triangleSubdivide vs
{-# INLINABLE spherePrimitive #-}

-- | Subdivide triangle into 4 triangles and adjust points to sphere surface
triangleSubdivide :: forall a . Unbox a => VertexTriangle a -> [VertexTriangle a]
triangleSubdivide (VertexTriangle v1 v2 v3 cval) = [
    mkTr v1  v12 v31
  , mkTr v31 v12 v23
  , mkTr v12 v2  v23
  , mkTr v31 v23 v3
  ]
  where
    mkTr a b c = let
      n = normalizev $ (vertexPosition b - vertexPosition a) `crossv` (vertexPosition c - vertexPosition a)
      a' = a { vertexNormal = n }
      b' = b { vertexNormal = n }
      c' = c { vertexNormal = n }
      in VertexTriangle a' b' c' cval
    half a b = vertexScale 0.5 . vertexNormalize $ interpolateVertex 0.5 a b
    v12 = half v1 v2
    v23 = half v2 v3
    v31 = half v3 v1
{-# INLINE triangleSubdivide #-}
