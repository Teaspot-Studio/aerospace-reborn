-- | Defines primitive for extruding basic shapes
module Aerospace.CSG.Extrude(
    ScaledPath
  , extrudedPrimitive
  ) where

import Aerospace.Polygon
import Aerospace.Quaternion
import Aerospace.Triangle
import Aerospace.Vector2
import Aerospace.Vector3
import Aerospace.Vertex
import Data.Foldable (foldl')
import Data.Vector.Unboxed (Vector, Unbox)

import qualified Data.Vector.Unboxed as V

-- | Path consists from segments with attached info about rotation and scale on each step
type ScaledPath = Vector (Vector3, Quaternion, Float)

-- | Calculate new vector for a step of 'ScaledPath'
scaledPathNext :: Vector3 -> Vector3 -> Quaternion -> Float -> Vector3 -> Vector3
scaledPathNext centroid offset rot scale v = v' + scaleOffset + offset
  where
    scaleOffset = scalev (v - centroid) (scale - 1)
    v' = centroid + quaternionRotate rot (v - centroid)

-- | Transform polygon with offset, rotation and scale
transformPolygon :: Vector3 -> Quaternion -> Float -> Polygon -> Polygon
transformPolygon v q s plg@(Polygon xs) = Polygon $ V.map f xs
  where
    centroid = polygonCentroid plg
    f = scaledPathNext centroid v q s

-- | Defines prism by basement polygon, path of extrusion with scales for each step.
-- This can produce cube, cylinder, prism, cone, torus and other figures.
extrudedPrimitive :: forall a . Unbox a => Polygon -> ScaledPath -> Bool -> a -> Vector (VertexTriangle a)
extrudedPrimitive plg ps closed a = V.fromList $ if closed
  then startTrs ++ midtrs ++ endTrs
  else midtrs ++ closingTrs
  where
    (eplg, midtrs) = V.foldl' mkStep (plg, []) ps
    mkStep (!aplg, !atrs) (offset, rot, scale) = let
      centroid = polygonCentroid aplg
      trs = concat $ fmap (extrudeSegment centroid offset rot scale a) $ polygonSegments aplg
      in (transformPolygon offset rot scale aplg, atrs ++ trs)
    startTrs = triangleFlip . mkTr <$> polygonTriangles plg
    endTrs = mkTr <$> polygonTriangles eplg
    closingTrs = enclosePolygons eplg plg a
    mkTr (v1, v2, v3) = let
      n = normalizev $ (v2 - v1) `crossv` (v3 - v1)
      in VertexTriangle (Vertex v1 n 0) (Vertex v2 n 0) (Vertex v3 n 0) a

-- | Extude single segment to produce two triangles
extrudeSegment :: forall a . Unbox a => Vector3 -> Vector3 -> Quaternion -> Float -> a -> (Vector3, Vector3) -> [VertexTriangle a]
extrudeSegment centroid offset rot scale av (v1, v2) = [
    mkTr v1 u1 v2  u2  v1' u1'
  , mkTr v2 u2 v2' u2' v1' u1'
  ]
  where
    v1' = scaledPathNext centroid offset rot scale v1
    v2' = scaledPathNext centroid offset rot scale v2
    u1  = Vector2 0 1
    u2  = Vector2 1 1
    u1' = Vector2 0 0
    u2' = Vector2 1 0
    mkTr a au b bu c cu = let
      n = normalizev $ (b - a) `crossv` (c - a)
      in VertexTriangle (Vertex a n au) (Vertex b n bu) (Vertex c n cu) av

-- | Connect two same-arity polygons with triangles
enclosePolygons :: forall a . Unbox a => Polygon -> Polygon -> a -> [VertexTriangle a]
enclosePolygons p1 p2 av = concat $ fmap encloseSegment $ polygonSegments p1 `zip` polygonSegments p2
  where
    encloseSegment ((v1, v2), (v1', v2')) = [
        mkTr v1 u1 v2  u2  v1' u1'
      , mkTr v2 u2 v2' u2' v1' u1'
      ]
    u1  = Vector2 0 1
    u2  = Vector2 1 1
    u1' = Vector2 0 0
    u2' = Vector2 1 0
    mkTr a au b bu c cu = let
      n = normalizev $ (b - a) `crossv` (c - a)
      in VertexTriangle (Vertex a n au) (Vertex b n bu) (Vertex c n cu) av
