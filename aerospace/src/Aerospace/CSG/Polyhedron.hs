-- | Defines primitives models for several polyhedrons
module Aerospace.CSG.Polyhedron(
    tetrahedronPrimitive
  , octahedronPrimitive
  , dodecahedronPrimitive
  , icosahedronPrimitive
  ) where

import Aerospace.Triangle
import Aerospace.Vector3
import Aerospace.Vertex
import Data.Vector.Unboxed (Vector, Unbox)

import qualified Data.Vector.Unboxed as V

-- | Ideal tetrahedron with center at origin and length of side equals 1
tetrahedronPrimitive :: forall a . Unbox a => a -> Vector (VertexTriangle a)
tetrahedronPrimitive a = V.fromList [
    mkTr v1 v3 v2
  , mkTr v1 v4 v3
  , mkTr v1 v2 v4
  , mkTr v2 v3 v4
  ]
  where
    v1 = Vector3   0.0        0.5        0.0
    v2 = Vector3   0.0      (-0.16665)   0.47140002
    v3 = Vector3 (-0.40825) (-0.16665) (-0.23570001)
    v4 = Vector3   0.40825  (-0.16665) (-0.23570001)
    mkVert v n = Vertex v n 0
    mkTr p1 p2 p3 = let n = mkNorm p1 p2 p3
      in VertexTriangle (mkVert p1 n) (mkVert p2 n) (mkVert p3 n) a
    mkNorm p1 p2 p3 = normalizev $ crossv (p2 - p1) (p3 - p1)
{-# INLINE tetrahedronPrimitive #-}

-- | Ideal octahedron with center at origin and length of side equals 1
octahedronPrimitive :: forall a . Unbox a => a -> Vector (VertexTriangle a)
octahedronPrimitive a = V.fromList [
    mkTr v2 v1 v5
  , mkTr v3 v2 v5
  , mkTr v4 v3 v5
  , mkTr v1 v4 v5
  , mkTr v1 v2 v6
  , mkTr v2 v3 v6
  , mkTr v3 v4 v6
  , mkTr v4 v1 v6
  ]
  where
    v1 = Vector3   0.5    0      0
    v2 = Vector3   0    (-0.5)   0
    v3 = Vector3 (-0.5)   0      0
    v4 = Vector3   0      0.5    0
    v5 = Vector3   0      0      0.5
    v6 = Vector3   0      0    (-0.5)
    mkNorm p1 p2 p3 = normalizev $ crossv (p2 - p1) (p3 - p1)
    mkVert v n = Vertex v n 0
    mkTr p1 p2 p3 = let n = mkNorm p1 p2 p3
      in VertexTriangle (mkVert p1 n) (mkVert p2 n) (mkVert p3 n) a
{-# INLINABLE octahedronPrimitive #-}

-- | Ideal dodecahedron with center at origin and length of side equals 1
dodecahedronPrimitive :: forall a . Unbox a => a -> Vector (VertexTriangle a)
dodecahedronPrimitive a = V.fromList [
    mkTr v1 v3 v2
  , mkTr v3 v1 v4
  , mkTr v2 v3 v5
  , mkTr v1 v7 v6
  , mkTr v7 v1 v2
  , mkTr v6 v7 v8
  , mkTr v1 v9 v4
  , mkTr v9 v1 v6
  , mkTr v4 v9 v10
  , mkTr v2 v11 v7
  , mkTr v11 v2 v5
  , mkTr v7 v11 v12
  , mkTr v6 v13 v9
  , mkTr v13 v6 v8
  , mkTr v9 v13 v14
  , mkTr v4 v15 v3
  , mkTr v15 v4 v10
  , mkTr v3 v15 v16
  , mkTr v5 v16 v11
  , mkTr v16 v5 v3
  , mkTr v11 v16 v17
  , mkTr v15 v9 v18
  , mkTr v9 v15 v10
  , mkTr v18 v9 v14
  , mkTr v13 v7 v19
  , mkTr v7 v13 v8
  , mkTr v19 v7 v12
  , mkTr v20 v12 v17
  , mkTr v12 v20 v19
  , mkTr v17 v12 v11
  , mkTr v20 v16 v18
  , mkTr v16 v20 v17
  , mkTr v18 v16 v15
  , mkTr v20 v14 v19
  , mkTr v14 v20 v18
  , mkTr v19 v14 v13
  ]
  where
    v1  = Vector3 (-0.2887) 0.2887 (-0.2887)
    v2  = Vector3 (-0.17840001) 0.0 (-0.4671)
    v3  = Vector3 0.2887 0.2887 (-0.2887)
    v4  = Vector3 0.0 0.4671 (-0.17840001)
    v5  = Vector3 0.17840001 0.0 (-0.4671)
    v6  = Vector3 (-0.4671) 0.17840001 0.0
    v7  = Vector3 (-0.2887) (-0.2887) (-0.2887)
    v8  = Vector3 (-0.4671) (-0.17840001) 0.0
    v9  = Vector3 (-0.2887) 0.2887 0.2887
    v10 = Vector3 0.0 0.4671 0.17840001
    v11 = Vector3 0.2887 (-0.2887) (-0.2887)
    v12 = Vector3 0.0 (-0.4671) (-0.17840001)
    v13 = Vector3 (-0.2887) (-0.2887) 0.2887
    v14 = Vector3 (-0.17840001) 0.0 0.4671
    v15 = Vector3 0.2887 0.2887 0.2887
    v16 = Vector3 0.4671 0.17840001 0.0
    v17 = Vector3 0.4671 (-0.17840001) 0.0
    v18 = Vector3 0.17840001 0.0 0.4671
    v19 = Vector3 0.0 (-0.4671) 0.17840001
    v20 = Vector3 0.2887 (-0.2887) 0.2887

    mkNorm p1 p2 p3 = normalizev $ crossv (p2 - p1) (p3 - p1)
    mkVert v n = Vertex v n 0
    mkTr p1 p2 p3 = let n = mkNorm p1 p2 p3
      in VertexTriangle (mkVert p1 n) (mkVert p2 n) (mkVert p3 n) a


-- | Ideal icosahedron with center at origin and length of side equals 1
icosahedronPrimitive :: forall a . Unbox a => a -> Vector (VertexTriangle a)
icosahedronPrimitive a = V.fromList [
    mkTr v1 v3 v2
  , mkTr v1 v4 v3
  , mkTr v1 v5 v4
  , mkTr v1 v6 v5
  , mkTr v1 v2 v6
  , mkTr v2 v3 v7
  , mkTr v3 v8 v7
  , mkTr v3 v4 v8
  , mkTr v4 v9 v8
  , mkTr v4 v5 v9
  , mkTr v5 v10 v9
  , mkTr v5 v6 v10
  , mkTr v6 v11 v10
  , mkTr v6 v2 v11
  , mkTr v2 v7 v11
  , mkTr v7 v8 v12
  , mkTr v8 v9 v12
  , mkTr v9 v10 v12
  , mkTr v10 v11 v12
  , mkTr v11 v7 v12
  ]
  where
    v1  = Vector3 0.0 0.5 0.0
    v2  = Vector3 0.0 0.22360002 0.44720003
    v3  = Vector3 (-0.42535) 0.22360002 0.1382
    v4  = Vector3 (-0.26285002) 0.22360002 (-0.36180001)
    v5  = Vector3 0.26285002 0.22360002 (-0.36180001)
    v6  = Vector3 0.42535 0.22360002 0.1382
    v7  = Vector3 (-0.26285002) (-0.22360002) 0.36180001
    v8  = Vector3 (-0.42535) (-0.22360002) (-0.1382)
    v9  = Vector3 0.0 (-0.22360002) (-0.44720003)
    v10 = Vector3 0.42535 (-0.22360002) (-0.1382)
    v11 = Vector3 0.26285002 (-0.22360002) 0.36180001
    v12 = Vector3 0.0 (-0.5) 0.0

    mkNorm p1 p2 p3 = normalizev $ crossv (p2 - p1) (p3 - p1)
    mkVert v n = Vertex v n 0
    mkTr p1 p2 p3 = let n = mkNorm p1 p2 p3
      in VertexTriangle (mkVert p1 n) (mkVert p2 n) (mkVert p3 n) a
{-# INLINABLE icosahedronPrimitive #-}
